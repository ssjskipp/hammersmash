package com.ssjskipp.games.hammersmash.bazaar;

/**
 * ...
 * @author Chris Federici
 * 
 * A Pool of items.
 * 
 * By default, it is fixed.
 */

class Pool<T> {
	
	var poolQueue:Array<T>;
	var freeIndex:Int;
	
	public function new(maxSize:Int = 100, fixed:Bool = true) {
		maxSize = Math.max(maxSize, 1);
		poolQueue = new Array<T>();
		for (i in 0...maxSize) {
			
		}
	}
	
	public function get():T {
		
	}
	
}