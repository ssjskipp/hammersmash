package com.ssjskipp.games.hammersmash.bazaar.assets;
//import com.ssjskipp.games.hammersmash.bazaar.maths.Rectangle;
import nme.geom.Rectangle;

/**
 * ...
 * @author ...
 */
class SpriteAsset extends Asset
{
	public static inline var TYPE:String = "spriteAsset";
	
	public var renderId:Int;
	public var spriteRect:Rectangle;
	public var width(getWidth, null):Float;
	public var height(getHeight, null):Float;
	
	public function getWidth():Float {
		return spriteRect.width;
	}
	
	public function getHeight():Float {
		return spriteRect.height;
	}

	public function new(rect:Rectangle, ID:Int) 
	{
		super(TYPE);
		spriteRect = rect;
		renderId = ID;
	}
	
	public function toString():String {
		return "[Sprite(id=" + renderId + ",rect=" + spriteRect + ")]";
	}
	
}