package com.ssjskipp.games.hammersmash.bazaar.assets;
import nme.display.BitmapData;

/**
 * ...
 * @author ...
 */
class BitmapAsset
{	
	public var bitmapData:BitmapData;

	public function new(bitmapDataSource:BitmapData) 
	{
		bitmapData = bitmapDataSource;
	}
	
}