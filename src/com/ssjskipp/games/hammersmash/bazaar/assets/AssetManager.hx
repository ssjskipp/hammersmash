package com.ssjskipp.games.hammersmash.bazaar.assets;
//import com.ssjskipp.games.hammersmash.bazaar.maths.Rectangle;
import nme.geom.Rectangle;
import com.ssjskipp.games.hammersmash.bazaar.render.TileRenderer;
import nme.Assets;
import nme.display.BitmapData;
import nme.geom.Point;
/**
 * ...
 * @author Chris Federici
 * 
 * Bazaar Engine Asset Manager. Use this to create objects that the engine understands.
 * 
 * This uses the nme Asset access for now. Can add other interfaces if you like I suppose.
 * Loaded assets are stored in an array.
 */

 enum AssetManagerType {
	 NME;
 }
 
class AssetManager {
	
	public var type:AssetManagerType;
	var loadedAssets:Hash<Dynamic>;
	
	public function new(managerType:AssetManagerType) {
		type = managerType;
		loadedAssets = new Hash<Dynamic>();
	}
	
	public function getAsset(assetId:String):Asset {
		return loadedAssets.get(assetId);
	}
	
	public function loadBitmapAsset(id:String, ?path:String):BitmapAsset {
		var data:BitmapData = null;
		var newAsset:BitmapAsset = cast getAsset(id);
		if (newAsset == null) {
			switch (type) {
				case NME:
					data = Assets.getBitmapData(path, true);
			}
			newAsset = new BitmapAsset(data);
			loadedAssets.set(path, newAsset);
		}
		return newAsset;
	}
	
	public function loadSpriteAsset(id:String, renderer:TileRenderer, rect:Rectangle, pt:Point):SpriteAsset {
		var data:Int;
		var asset:Asset = loadedAssets.get(id);
		if (asset != null && asset.assetType != SpriteAsset.TYPE) {
			trace("Error. Asset type (" + asset.assetType + ") is not a SpriteAsset.");
			return null;
		}
		
		var newAsset:SpriteAsset = cast asset;
		if (newAsset == null && renderer != null && rect != null) {
			data = renderer.addRect(rect, pt);
			newAsset = new SpriteAsset(rect, data);
			loadedAssets.set(id, newAsset);
		}
		
		return newAsset;
	}
	
}