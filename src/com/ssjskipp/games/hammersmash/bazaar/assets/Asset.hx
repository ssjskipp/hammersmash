package com.ssjskipp.games.hammersmash.bazaar.assets;

/**
 * ...
 * @author Chris Federici
 * 
 * Generic class for an asset. Wraps a type and data.
 * An AssetManager controls how data is interpreted.
 */
 
class Asset {
	
	public var assetType:String;
	public var data:Dynamic;
	
	public function new(type:String, ?data:Dynamic) {
		this.data = data;
		assetType = type;
	}
	
}