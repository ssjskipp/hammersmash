package com.ssjskipp.games.hammersmash.bazaar.scene;
import com.ssjskipp.games.hammersmash.bazaar.assets.BitmapAsset;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import com.ssjskipp.games.hammersmash.bazaar.render.CameraController;
import com.ssjskipp.games.hammersmash.bazaar.render.TileRenderer;
import com.ssjskipp.games.hammersmash.game.HammerSmashGame;
import com.ssjskipp.games.hammersmash.game.misc.Projectile;
import nme.display.Bitmap;
import nme.display.Graphics;
import nme.display.Shape;
import nme.display.Sprite;
import nme.display.Stage;
import nme.events.Event;
import nme.Lib;

/**
 * ...
 * @author Chris Federici
 * 
 * manages actors on a scene, specific to a renderer.
 * 
 * This is the interface between the HammerSmashGame and its renderer, in a way.
 * 
 */

class SceneManager 
{
	
	public static inline var FRAME_RATE:Float = 1 / 30;//Frame rate is for animation mostly. Physics based should be off of DT, not a scale of frame rate.
	public static var REF:SceneManager;
	
	public var stage:Stage;
	
	public var elapsedTime(default, null):Float;
	public var isPaused:Bool = true;
	
	public var render:TileRenderer;
	public var camera:CameraController;
	
	var gameRenderTarget:Graphics;
	
	var actors:List<Actor>;
	var toRemoveActors:List<Actor>;
	var toAddActors:List<Actor>;

	var tickables:List<ITickable>;
	var toRemoveTickable:List<ITickable>;
	var toAddTickable:List<ITickable>;
	
	//Somehow, collisions should be a thing
	
	public function new(stage:Stage, renderTarget:Sprite, tileRenderTarget:Graphics, renderEngine:TileRenderer) 
	{
		var sheetData:String;
		
		REF = this;
		
		this.stage = stage;
		
		actors = new List<Actor>();
		toRemoveActors = new List<Actor>();
		toAddActors = new List<Actor>();
		
		tickables = new List<ITickable>();
		toRemoveTickable = new List<ITickable>();
		toAddTickable = new List<ITickable>();
		
		elapsedTime = Lib.getTimer() / 1000;
		
		render = renderEngine;
		gameRenderTarget = tileRenderTarget;
		
		camera = new CameraController(renderTarget);
		tickables.add(camera);
		
		stage.addEventListener(Event.ENTER_FRAME, update);
	}
	
	public function addActor(actor:Actor):Void {
		actor.parent = this;
		toAddActors.add(actor);
	}
	public function removeActor(actor:Actor):Void {
		actor.parent = null;
		toRemoveActors.add(actor);
	}
	
	public function addTickable(tickable:ITickable):Void {
		toAddTickable.add(tickable);
	}
	public function removeTickable(tickable:ITickable):Void {
		toRemoveTickable.add(tickable);
	}
	
	public function update(e:Event):Void {
		var currentTime:Float = Lib.getTimer() / 1000;
		var delta:Float = currentTime - elapsedTime;
		var step:Float = delta / FRAME_RATE;
		
		elapsedTime = currentTime;
		
		if (isPaused) return;
		
		for (actor in toAddActors) { actors.add(actor); }
		for (tick in toAddTickable) { tickables.add(tick); }
		toAddActors.clear();
		toAddTickable.clear();
		
		for (actor in toRemoveActors) { actors.remove(actor); }
		for (tick in toRemoveTickable) { tickables.remove(tick); }
		toRemoveActors.clear();
		toRemoveTickable.clear();
		
		//HACKISH CLEAR API
		HammerSmashGame.REF.gameApi.graphics.clear();
		
		//Tick block
		for (tock in tickables) {
			if (tock == null) {
				removeTickable(tock);
			} else {
				if (tock.enabled) tock.tick(delta, step);
			}
		}
		
		//HammerSmashGame.REF.qt.debugDraw(HammerSmashGame.REF.gameApi.graphics);
		
		//Enter Render
		//Need to figure out a GUI or something?
		gameRenderTarget.clear();
		render.postRender(gameRenderTarget);
	}
}