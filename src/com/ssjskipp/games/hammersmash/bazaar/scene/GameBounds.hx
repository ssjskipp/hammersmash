package com.ssjskipp.games.hammersmash.bazaar.scene;
import nme.geom.Point;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * (0, 0) is the top left of game world, (worldWidth, worldHeight) is the bottom right of the game world.
 * Currently, the camera is zoomed to show the entire game world all the time.
 * 
 * Use scaleFactor to scale UP the game world to fit.
 * The paddings are used for collision bounds. These are taken into account for getting left, top, bottom, and right.
 * 
 * worldWidth = width of entire game world display
 * worldHeight = height of entire game world display
 * 
 * left, right, top, bottom, width, height, centerX, centerY all refer to playable game area.
 */
class GameBounds
{

	public var worldWidth:Float = 0;
	public var worldHeight:Float = 0;
	public var screenWidth:Float = 0;
	public var screenHeight:Float = 0;
	public var paddingLeft:Float = 20;
	public var paddingRight:Float = 20;
	public var paddingTop:Float = 20;
	public var paddingBottom:Float = 20;
	
	//These get filled with the value needed to move the game screen in screen space (top and left) so the scaled output is centered.
	public var offsetLeft:Float = 0;
	public var offsetTop:Float = 0;
	
	//Things that are calculated on the fly.
	//These are the "playable game area" variables.
	//Meaning, don't use these to arrange UI elements.
	//AKA, anything that you multiply with scaleFactor, use these as your boundings
	public var left(getGameLeft, null):Float;
	public var right(getGameRight, null):Float;
	public var top(getGameTop, null):Float;
	public var bottom(getGameBottom, null):Float;
	public var width(getGameWidth, null):Float;
	public var height(getGameHeight, null):Float;
	public var centerX(getCenterX, null):Float;
	public var centerY(getCenterY, null):Float;
	
	public var scaleFactor:Float;//Factor to scale width and height by to fill the screen in the best way
	public var aspectRatio:Float;//width / height, aspectRatio * height = desired width at that height
	
	public function new(internalWidth:Float, internalHeight:Float, externalWidth:Float, externalHeight:Float) {
		worldWidth = internalWidth;
		worldHeight = internalHeight;
		screenWidth = externalWidth;
		screenHeight = externalHeight;
		
		//Get aspect ratio, useful for calculating things, sometimes.
		aspectRatio = worldWidth / worldHeight;
		scaleFactor = Math.min(screenWidth / worldWidth, screenHeight / worldHeight);
		
		offsetLeft = (screenWidth - worldWidth * scaleFactor) / 2;
		offsetTop = (screenHeight - worldHeight * scaleFactor) / 2;
	}
	
	public function screenToGameX(value:Float):Float { return (value - offsetLeft) / scaleFactor; }
	public function screenToGameY(value:Float):Float { return (value - offsetTop) / scaleFactor; }
	
	//These return in reference to the game world.
	//That is, use these values as bounds to position elements.
	//Padding is walls, edges of game world.
	public function getGameLeft():Float { return paddingLeft; }
	public function getGameRight():Float { return worldWidth - paddingRight; }
	public function getGameTop():Float { return paddingTop; }
	public function getGameBottom():Float { return worldHeight - paddingBottom; }
	public function getGameWidth():Float { return worldWidth - paddingLeft - paddingRight; }
	public function getGameHeight():Float { return worldHeight - paddingTop - paddingBottom; }
	public function getCenterX():Float { return paddingLeft + 0.5 * getGameWidth(); }
	public function getCenterY():Float { return paddingTop + 0.5 * getGameHeight(); }
	
	public function toString():String {
		return "[Game(" + worldWidth + "," + worldHeight + "),Screen(" + screenWidth + "," + screenHeight + "),Offsets(" + offsetLeft + "," + offsetTop + ")]";
	}
}