package com.ssjskipp.games.hammersmash.bazaar.scene;

import com.ssjskipp.games.hammersmash.bazaar.assets.SpriteAsset;
import com.ssjskipp.games.hammersmash.bazaar.collide.CollideRect;
import com.ssjskipp.games.hammersmash.bazaar.collide.ICollidable;
import com.ssjskipp.games.hammersmash.bazaar.collide.quadtree.IQuadTreeItem;
import com.ssjskipp.games.hammersmash.bazaar.collide.quadtree.QuadTree;
import com.ssjskipp.games.hammersmash.bazaar.misc.IPositionable;
import com.ssjskipp.games.hammersmash.bazaar.physics.ProjectilePhysics;
//import com.ssjskipp.games.hammersmash.bazaar.maths.Rectangle;
import nme.geom.Rectangle;
import com.ssjskipp.games.hammersmash.bazaar.render.TileRenderer;
import com.ssjskipp.games.hammersmash.bazaar.render.TileRenderItem;
import com.ssjskipp.games.hammersmash.bazaar.render.TileRenderList;
import nme.events.Event;
import nme.events.EventDispatcher;

/**
 * ...
 * @author Chris Federici
 * 
 * An Actor is a sprite with things like Physics and other components
 * 
 * It's basically a Graphics and some data
 */

class Actor extends EventDispatcher, implements IPositionable {
	
	public static inline var DESTROY_EVENT:String = "destroy";
	
	public var parent:SceneManager;
	public var sprite:TileRenderItem;
	public var x(getX, setX):Float = 0;
	public var y(getY, setY):Float = 0;
	public var width(getWidth, null):Float;
	public var height(getHeight, null):Float;
	public var rotation(getRotation, setRotation):Float;
	public var projectilePhysics:ProjectilePhysics;
	
	public var properties:Hash<Dynamic>;
	
	//MOVE THIS TO ITS OWN THING
	public var gravityX:Float;
	public var gravityY:Float;
	
	//Collision
	public var collisionRect:CollideRect;
	public var quadTreeItem:IQuadTreeItem;
	public var radius:Float = 0;
	
	public var isDestroyed:Bool;
	
	//Getter/Setter Blockz
	public function getWidth():Float { return sprite.width; }
	public function getHeight():Float { return sprite.height; }
	public function getX():Float { return sprite.x; }
	public function getY():Float { return sprite.y; }
	public function getRotation():Float { return sprite.rotation; }
	public function setX(newVal:Float):Float {
		if (collisionRect != null) collisionRect.x = newVal - collisionRect.width / 2;
		return sprite.x = newVal;
	}
	public function setY(newVal:Float):Float {
		if (collisionRect != null) collisionRect.y = newVal - collisionRect.height / 2;
		return sprite.y = newVal;
	}
	public function setRotation(newVal:Float):Float { return sprite.rotation = newVal; }
	
 	public function new(sprite:TileRenderItem, ?customRect:Rectangle) {
		super();
		
		isDestroyed = false;
		
		properties = new Hash<Dynamic>();
		
		this.sprite = sprite;
		gravityX = gravityY = 0;
		
		radius = Math.sqrt(width * width + height * height) / 2;
		
		if (customRect == null) {
			customRect = new Rectangle(0, 0, width, height);
		}
		this.collisionRect = new CollideRect(sprite.x - customRect.width/2, sprite.y - customRect.height/2, customRect.width, customRect.height);
	}
	
	//Also cleans up internal objects
	public function destroy():Void {
		if (!isDestroyed) isDestroyed = true;
		if (quadTreeItem != null) QuadTree.RemoveItem(quadTreeItem);
		if (parent != null) parent.removeActor(this);
		TileRenderer.REF.renderList.removeItem(sprite);
	}
	
	public function setPosition(?x:Float, ?y:Float):Void {
		if (x != null) setX(x);
		if (y != null) setY(y);
		if (quadTreeItem != null) quadTreeItem.setPosition(sprite.x, sprite.y);
	}
	
	public inline function hasProjectilePhysics():Bool { return projectilePhysics != null; }
	
	public function enableProjectilePhysics():Void {
		if (projectilePhysics == null) projectilePhysics = new ProjectilePhysics();
	}
	
}