package com.ssjskipp.games.hammersmash.bazaar.controllers;

import com.ssjskipp.games.hammersmash.bazaar.events.ButtonEvent;
import nme.events.Event;
import nme.events.EventDispatcher;
import nme.events.KeyboardEvent;
import nme.Lib;

/**
 * ...
 * @author Chris Federici
 * 
 * Represents a connected game controller.
 * 
 * This only applies to key/button input.
 * 
 * The Pointer class represents a touch/mouse interface.
 */

enum ButtonInputTypes {
	Keyboard;
}
 
class ButtonInput extends EventDispatcher
{
	
	//Redefine these as you please?
	public static inline var MOVE_UP:String = "moveUp";
	public static inline var MOVE_DOWN:String = "moveDown";
	public static inline var MOVE_LEFT:String = "moveLeft";
	public static inline var MOVE_RIGHT:String = "moveRight";
	public static inline var SPRINT:String = "sprint";
	public static inline var INTERACT:String = "interact";
	
	public var controlMap:Hash<String>;
	public var buttonStatus:Hash<Bool>;
	public var controllerType:ButtonInputTypes;
	
	public function new(type:ButtonInputTypes) {
		super();
		
		controllerType = type;
		controlMap = new Hash<String>();
		buttonStatus = new Hash<Bool>();
		
		switch (type) {
			case ButtonInputTypes.Keyboard:
				Lib.stage.addEventListener(KeyboardEvent.KEY_DOWN, inputListen);
				Lib.stage.addEventListener(KeyboardEvent.KEY_UP, inputListen);
				controlMap.set("87", MOVE_UP);
				controlMap.set("119", MOVE_UP);
				controlMap.set("83", MOVE_DOWN);
				controlMap.set("115", MOVE_DOWN);
				controlMap.set("65", MOVE_LEFT);
				controlMap.set("97", MOVE_LEFT);
				controlMap.set("68", MOVE_RIGHT);
				controlMap.set("100", MOVE_RIGHT);
				controlMap.set("16", SPRINT);
				controlMap.set("69", INTERACT);
				controlMap.set("101", INTERACT);
				buttonStatus.set(MOVE_UP, false);
				buttonStatus.set(MOVE_DOWN, false);
				buttonStatus.set(MOVE_LEFT, false);
				buttonStatus.set(MOVE_RIGHT, false);
				buttonStatus.set(SPRINT, false);
				buttonStatus.set(INTERACT, false);
		}
	}
	
	public function inputListen(event:Event):Void {
		switch (controllerType) {
			case ButtonInputTypes.Keyboard:
				var keyboardEvent:KeyboardEvent = cast event;
				var keyCode:String = cast keyboardEvent.keyCode;
				var keyConvert:String = controlMap.get(keyCode);
				switch (keyConvert) {
					case MOVE_UP, MOVE_DOWN, MOVE_LEFT, MOVE_RIGHT, SPRINT, INTERACT:
						buttonStatus.set(keyConvert, event.type == KeyboardEvent.KEY_DOWN);
						dispatchEvent(new ButtonEvent(event.type == KeyboardEvent.KEY_DOWN ? ButtonEvent.PRESS : ButtonEvent.RELEASE, keyConvert));
				}
		}
	}
	
	public inline function isDown(button:String):Bool { return buttonStatus.get(button); }
	
}