package com.ssjskipp.games.hammersmash.bazaar.controllers;

import com.ssjskipp.games.hammersmash.bazaar.debug.Debug;
import com.ssjskipp.games.hammersmash.bazaar.debug.DebugChannels;
import com.ssjskipp.games.hammersmash.bazaar.events.PointerEvent;
import com.ssjskipp.games.hammersmash.bazaar.scene.GameBounds;
import nme.events.Event;
import nme.events.EventDispatcher;
import nme.events.MouseEvent;
import nme.events.TouchEvent;
import nme.Lib;

/**
 * ...
 * @author Chris Federici
 * 
 * Represents a pointer. On mobile, this is touch events, on computers, mouse pointers.
 */

enum PointerType {
	Mouse;
	Touch;
}

enum PointerState {
	Up;
	Down;
	Held;
	Dragging;
}

class Pointer extends EventDispatcher {
	
	public var x(default, null):Float = 0;
	public var y(default, null):Float = 0;
	public var state(default, null):PointerState;
	
	public var pointerType:PointerType;
	public var gameBounds:GameBounds;
	
	public function new(type:PointerType, bounds:GameBounds) {
		super();
		
		gameBounds = bounds;
		pointerType = type;
		state = PointerState.Up;
		
		Debug.log(DebugChannels.ENGINE, "New pointer of type: " + pointerType);
		
		switch (pointerType) {
			case Mouse:
				Lib.stage.addEventListener(MouseEvent.MOUSE_MOVE, inputListen);
				Lib.stage.addEventListener(MouseEvent.MOUSE_DOWN, inputListen);
				Lib.stage.addEventListener(MouseEvent.MOUSE_UP, inputListen);
			case Touch:
				Lib.stage.addEventListener(TouchEvent.TOUCH_BEGIN, inputListen);
				Lib.stage.addEventListener(TouchEvent.TOUCH_END, inputListen);
				Lib.stage.addEventListener(TouchEvent.TOUCH_MOVE, inputListen);
		}
	}
	
	public function inputListen(event:Event):Void {
		switch (pointerType) {
			case Mouse:
				var eventCast:MouseEvent = cast event;
				x = gameBounds.screenToGameX(eventCast.stageX);
				y = gameBounds.screenToGameY(eventCast.stageY);
				if (event.type == MouseEvent.CLICK) dispatchEvent(new PointerEvent(PointerEvent.CLICK, 0));
				if (event.type == MouseEvent.MOUSE_DOWN) {
					state = PointerState.Down;
					dispatchEvent(new PointerEvent(PointerEvent.PRESS, 0));
				}
				if (event.type == MouseEvent.MOUSE_UP) {
					state = PointerState.Up;
					dispatchEvent(new PointerEvent(PointerEvent.RELEASE, 0));
				}
				if (event.type == MouseEvent.MOUSE_MOVE) dispatchEvent(new PointerEvent(PointerEvent.DRAG, 0));
			case Touch:
				var touchEvent:TouchEvent = cast event;
				x = gameBounds.screenToGameX(touchEvent.stageX);
				y = gameBounds.screenToGameY(touchEvent.stageY);
				if (event.type == TouchEvent.TOUCH_BEGIN) {
					state = PointerState.Down;
					dispatchEvent(new PointerEvent(PointerEvent.PRESS, touchEvent.touchPointID));
				}
				if (event.type == TouchEvent.TOUCH_END) {
					state = PointerState.Up;
					dispatchEvent(new PointerEvent(PointerEvent.RELEASE, touchEvent.touchPointID));
				}
				if (event.type == TouchEvent.TOUCH_MOVE) dispatchEvent(new PointerEvent(PointerEvent.DRAG, touchEvent.touchPointID));
		}
	}
	
	override public function toString():String {
		return "[(Pointer)(x=" + x + ",y=" + y + ",state=" + state+")]";
	}
	
}