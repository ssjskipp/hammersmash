package com.ssjskipp.games.hammersmash.bazaar.collide.quadtree;
import com.ssjskipp.games.hammersmash.bazaar.collide.quadtree.IQuadTreeItem;
import com.ssjskipp.games.hammersmash.bazaar.debug.Debug;
//import com.ssjskipp.games.hammersmash.bazaar.maths.Rectangle;
import nme.geom.Rectangle;
import nme.display.Graphics;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Quadtree represents a way to spacially divide the objects on the screen.
 * A quadtree can either have children with no contents, or contents with no children.
 * That is, you can either contain a list of items, or 4 more quad trees.
 * 
 * FOR NOW ONLY INTERACT USING THE ROOT TREE
 */
class QuadTree {

	//A quad with NODE_CAPACITY+1 items will split into 2 quads
	public static inline var NODE_CAPACITY:Int = 5;
	public static inline var MAX_TREE_DEPTH:Int = 4;
	
	public static inline var TOP_LEFT:Int = 0;
	public static inline var TOP_RIGHT:Int = 1;
	public static inline var BOTTOM_LEFT:Int = 2;
	public static inline var BOTTOM_RIGHT:Int = 3;
	
	public var boundary(default, null):Rectangle;
	public var isSplit(default, null):Bool = false;
	
	var contents:List<IQuadTreeItem>;
	var numContents:Int = 0;
	var depth:Int = 0;
	
	var subQuads:Array<QuadTree>;
	
	var parent:QuadTree;
	
	public function new(boundayBox:Rectangle) {
		boundary = boundayBox.clone();
		contents = new List<IQuadTreeItem>();
	}
	
	//Insert a new item at (x, y). Returns true/false on success/fail
	public function insert(item:IQuadTreeItem):Bool {
		//Do not insert if it's not inside.
		if (item == null || !boundary.contains(item.x, item.y)) return false;
		
		if (isSplit) {
			for (child in subQuads){
				if (child.insert(item)) break;
			}
		} else {
			contents.add(item);
			item.parent = this;
			if (depth < MAX_TREE_DEPTH && contents.length > NODE_CAPACITY) {
				split();
			}
		}
		
		numContents++;
		return true;
	}
	
	//Static function to remove an item from a QuadTree
	public static function RemoveItem(item:IQuadTreeItem):Bool {
		var parentTree:QuadTree = item.parent;
		if (parentTree == null || parentTree.contents == null) return false;
		
		parentTree.contents.remove(item);
		item.parent = null;
		
		while (parentTree != null) {
			parentTree.numContents--;
			if (parentTree.numContents < NODE_CAPACITY && parentTree.isSplit) {
				parentTree.merge();
			}
			parentTree = parentTree.parent;
		}
		
		return true;
	}
	
	//Returns all items inside a range
	public function queryRange(range:Rectangle, ?recursiveList:List<IQuadTreeItem>):List<IQuadTreeItem> {
		if (recursiveList == null) {
			recursiveList = new List<IQuadTreeItem>();
		}
		
		if (boundary.intersects(range)) {
			if (contents == null) {
				for (child in subQuads){
					child.queryRange(range, recursiveList);
				}
			} else if (contents.length > 0) {
				for (item in contents) {
					if (boundary.contains(item.x, item.y)) {
						recursiveList.add(item);
					}
				}
			}
		}
		
		return recursiveList;
	}
	
	//Update an item, it may have moved out of a quadtree.
	public function updateItem(item:IQuadTreeItem):Void {
		if (item.parent == null) return;
		if (!item.parent.boundary.contains(item.x, item.y)) {
			//I'm no longer in my parent QuadTree... Better find where I live now.
			//This can be reduced to 4 checks, based on where I leave.
			//But for now, naieve implementation. Just add to root.
			RemoveItem(item);
			var root:QuadTree = this;
			while (root.parent != null) {
				root = root.parent;
			}
			root.insert(item);
		}
	}
	
	function split():Void {
		//No need to subdivide if I'm not full, or if I've already divided.
		if (isSplit || contents.length <= NODE_CAPACITY) return;
		var box:Rectangle = boundary.clone();
		
		subQuads = new Array<QuadTree>();
		subQuads.push(null);
		subQuads.push(null);
		subQuads.push(null);
		subQuads.push(null);
		
		box.width /= 2;
		box.height /= 2;
		
		for (i in 0...4) {
			if (i == TOP_LEFT) {
				box.x = boundary.x;
				box.y = boundary.y;
			} else if (i == TOP_RIGHT) {
				box.x = boundary.x + box.width;
				box.y = boundary.y;
			} else if (i == BOTTOM_LEFT) {
				box.x = boundary.x;
				box.y = boundary.y + box.height;
			} else if (i == BOTTOM_RIGHT) {
				box.x = boundary.x + box.width;
				box.y = boundary.y + box.height;
			}
			subQuads[i] = new QuadTree(box);
			subQuads[i].parent = this;
			subQuads[i].depth = depth + 1;
		}
		
		//Re-allocate my items
		for (item in contents) {
			if (item == null) continue;
			item.parent = null;
			for (child in subQuads){
				if (child.insert(item)) break;
			}
		}
		contents = null;
		isSplit = true;
	}
	
	function merge():Void {
		if (isSplit) {
			contents = new List<IQuadTreeItem>();
			for (child in subQuads) {
				for (item in child.contents){
					contents.add(item);
					item.parent = this;
				}
				child.contents = null;
				child = null;
			}
			subQuads = null;
			isSplit = false;
		}
	}
	
	public function debugDraw(gfx:Graphics, level:Int = 1):Void {
		gfx.lineStyle(1, 0x0000FF);
		if (!isSplit) gfx.drawCircle(boundary.x, boundary.y, contents.length * 2 + 2);
		gfx.lineStyle(1, 0x00FF00);
		gfx.drawCircle(boundary.x, boundary.y, numContents*2 + 1);
		gfx.lineStyle(1, 0xFF0000);
		gfx.drawRect(boundary.x, boundary.y, boundary.width, boundary.height);
		if (subQuads != null){
			for (child in subQuads) {
				if (child != null) child.debugDraw(gfx, level + 1);
			}
		}
	}
}