package com.ssjskipp.games.hammersmash.bazaar.collide.quadtree;
import com.ssjskipp.games.hammersmash.bazaar.collide.CollideBall;
import com.ssjskipp.games.hammersmash.bazaar.collide.ICollidable;
import com.ssjskipp.games.hammersmash.bazaar.misc.IPositionable;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 */
class QuadTreeCollideBall implements IQuadTreeItem
{
	
	public static inline var ID:String = "QuadTreeCollideBall";

	public var attachedActor:Actor;
	public var attachedItem:IPositionable;
	public var attachedCollide:ICollidable;
	
	public var x(getX, null):Float;
	public var y(getY, null):Float;
	public var radius:Float;
	public var parent:QuadTree;
	
	public var itemId:String = ID;
	
	public function new(?attachedActor:Actor, x:Float = 0, y:Float = 0, radius:Float = 0) {
		if (attachedActor != null) {
			this.attachedActor = attachedActor;
			x = attachedActor.x;
			y = attachedActor.y;
			radius = attachedActor.radius;
		}
		attachedItem = new CollideBall(x, y, radius);
		attachedCollide = cast attachedItem;
		this.radius = radius;
	}
	
	public function setPosition(?x:Float, ?y:Float):Void {
		if (x != null) attachedItem.x = x;
		if (y != null) attachedItem.y = y;
		if (parent != null) parent.updateItem(this);
	}
	
	public function getX():Float {
		return attachedItem.x;
	}
	
	public function getY():Float {
		return attachedItem.y;
	}
	
	public function destroy():Void {
		QuadTree.RemoveItem(this);
		if (attachedActor != null) attachedActor.destroy();
	}
	
}