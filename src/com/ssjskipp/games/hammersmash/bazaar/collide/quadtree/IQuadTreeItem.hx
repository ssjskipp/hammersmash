package com.ssjskipp.games.hammersmash.bazaar.collide.quadtree;

import com.ssjskipp.games.hammersmash.bazaar.collide.ICollidable;
import com.ssjskipp.games.hammersmash.bazaar.misc.IPositionable;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * General item for a QuadTree.
 * 
 * Note that you 
 */
interface IQuadTreeItem {

	var attachedActor:Actor;
	var attachedItem:IPositionable;
	var x(getX, null):Float;
	var y(getY, null):Float;
	var parent:QuadTree;
	var itemId:String;
	
	function setPosition(?x:Float, ?y:Float):Void;
	function getX():Float;
	function getY():Float;
	
	//Remove from whatever quad tree + clean self up
	function destroy():Void;
}