package com.ssjskipp.games.hammersmash.bazaar.collide;
//import com.ssjskipp.games.hammersmash.bazaar.maths.Rectangle;
import nme.geom.Rectangle;

/**
 * ...
 * @author Chris Federici
 */

class CollideRect extends Rectangle, implements ICollidable
{
	
	public static inline var ID:String = "CollisionRect";
	
	public var enabled:Bool = true;
	public var collisionId:String = ID;

	public function new(?x:Float, ?y:Float, ?width:Float, ?height:Float) 
	{
		super(x, y, width, height);
	}
	
	public function doRectCollision(target:CollideRect):CollisionData {
		var data:CollisionData = new CollisionData();
		
		if (intersects(target)) {
			data.doesCollide = true;
			data.overlapRect = null;
			//data.overlapRect = intersection(target);
		}
		
		return data;
	}
	
}