package com.ssjskipp.games.hammersmash.bazaar.collide;

/**
 * ...
 * @author Chris Federici
 * 
 */

interface ICollidable 
{

	public var enabled:Bool;
	public var collisionId:String;
	
}