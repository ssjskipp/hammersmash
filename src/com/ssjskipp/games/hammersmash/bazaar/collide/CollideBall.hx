package com.ssjskipp.games.hammersmash.bazaar.collide;
import com.ssjskipp.games.hammersmash.bazaar.misc.IPositionable;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 */
class CollideBall implements ICollidable, implements IPositionable
{
	
	public static inline var ID:String = "CollisionBall";

	public var x(getX, setX):Float;
	public var y(getY, setY):Float;
	public var radius:Float;
	
	public var enabled:Bool;
	public var collisionId:String = ID;
	
	public function new(x:Float = 0, y:Float = 0, radius:Float = 0) {
		this.x = x;
		this.y = y;
		this.radius = radius;
	}
	
	public function setX(newValue:Float):Float { return x = newValue; }
	public function setY(newValue:Float):Float { return y = newValue; }
	public function getX():Float { return x; }
	public function getY():Float { return y; }
	
	public function doBallCollision(target:CollideBall):CollisionData {
		var data:CollisionData = new CollisionData();
		
		var xDistance:Float = target.x - x;
		var yDistance:Float = target.y - y;
		var distance:Float = Math.sqrt(xDistance * xDistance + yDistance * yDistance);
		
		if (radius + target.radius <= distance) {
			data.doesCollide = true;
			data.overlapRect = null;
		} else {
			data.doesCollide = false;
		}
		
		return data;
	}
}