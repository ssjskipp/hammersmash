package com.ssjskipp.games.hammersmash.bazaar.collide;
//import com.ssjskipp.games.hammersmash.bazaar.maths.Rectangle;
import nme.geom.Rectangle;

/**
 * ...
 * @author Chris Federici
 */

class CollisionData 
{

	public var doesCollide:Bool = false;
	public var overlapRect:Rectangle = null;
	
	public function new() 
	{
		
	}
	
}