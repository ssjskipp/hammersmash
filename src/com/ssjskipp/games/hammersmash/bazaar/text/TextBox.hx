package com.ssjskipp.games.hammersmash.bazaar.text;
import nme.Assets;
import nme.display.Sprite;
import nme.Lib;
import nme.text.Font;
import nme.text.TextField;
import nme.text.TextFormat;
import nme.text.TextFormatAlign;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * A simple text box that lives on the NME Display Object tree.
 */
class TextBox extends Sprite {
	
	public static inline var FONT_ANAHEIM:String = "fonts/anaheim.ttf";
	
	public var textField:TextField;
	
	public function new() {
		super();
		
		setup();
	}
	
	public function setup():Void {
		textField = new TextField();
		textField.cacheAsBitmap = true;
		
		//Fine defaults.
		var font:Font = Assets.getFont("fonts/anaheim.ttf");
		var format:TextFormat = new TextFormat(font.fontName, 12, 0xFFFFFF);
		textField.defaultTextFormat = format;
		textField.text = "";
		
		//Things that are useful to have as default
		textField.selectable = false;
		textField.multiline = true;
		textField.wordWrap = true;
		textField.mouseEnabled = false;
		
		this.addChild(textField);
		
		this.mouseChildren = false;
		this.mouseEnabled = false;
	}
	
	public function setDimensions(x:Float, y:Float, width:Float, height:Float):Void {
		textField.width = width;
		textField.height = height;
		this.x = x;
		this.y = y;
	}
	
	public function setText(string:String) {
		textField.text = string;
	}
	
	public function setFormat(fontName:String, ?size:Float, ?color:Int, ?bold:Bool, ?align:TextFormatAlign, ?alignStr:String):Void {
		var font:Font = Assets.getFont(fontName);
		var format:TextFormat = new TextFormat();
		format.font = font.fontName;
		format.color = color;
		format.size = size;
		format.bold = bold;
		#if flash
			format.align = align;
		#else
			format.align = alignStr;
		#end
		
		textField.defaultTextFormat = format;
		textField.setTextFormat(format);
	}
}