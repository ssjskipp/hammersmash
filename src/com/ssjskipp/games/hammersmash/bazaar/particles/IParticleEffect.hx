package com.ssjskipp.games.hammersmash.bazaar.particles;

/**
 * ...
 * @author Chris Federici
 */

interface IParticleEffect 
{
	
	public function effect(particle:Particle, deltaTime:Float):Void;
	
}