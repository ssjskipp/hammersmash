package com.ssjskipp.games.hammersmash.bazaar.ui.component;
import com.ssjskipp.games.hammersmash.bazaar.controllers.Pointer;
import com.ssjskipp.games.hammersmash.bazaar.events.PointerEvent;
//import com.ssjskipp.games.hammersmash.bazaar.maths.Rectangle;
import nme.geom.Rectangle;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.bazaar.ui.Menu;
import com.ssjskipp.games.hammersmash.bazaar.ui.UIComponent;
import com.ssjskipp.games.hammersmash.bazaar.ui.UIEvent;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Not defining the hitBox uses the width and height of the supplied upGraphic
 * 
 * listens to
 */

class Button extends UIComponent {
	
	public var downGraphic:Actor;
	public var upGraphic:Actor;
	public var hitBox:Rectangle;
	
	var pointer:Pointer;
	
	public function new(downGraphic:Actor, upGraphic:Actor, targetPointer:Pointer, ?customHitBox:Rectangle):Void {
		super(upGraphic);
		
		this.upGraphic = upGraphic;
		this.downGraphic = downGraphic;
		this.pointer = targetPointer;
		
		hitBox = (customHitBox == null) ? new Rectangle(-upGraphic.sprite.width/2, -upGraphic.sprite.height/2, upGraphic.sprite.width, upGraphic.sprite.height) : customHitBox;
		
		pointer.addEventListener(PointerEvent.PRESS, pointerListen);
		pointer.addEventListener(PointerEvent.RELEASE, pointerListen);
	}
	
	private function pointerListen(e:PointerEvent):Void {
		if (e.type == PointerEvent.PRESS) {
			if (hitBox.contains(pointer.x - graphic.x, pointer.y - graphic.y)) {
				graphic = downGraphic;
			}
		} else if (e.type == PointerEvent.RELEASE) {
			if (hitBox.contains(pointer.x - graphic.x, pointer.y - graphic.y)) {
				dispatchEvent(new UIEvent(UIEvent.BUTTON_CLICK, this));
			}
			graphic = upGraphic;
		}
	}
	
	override public function destroy():Void {
		if (pointer != null) {
			pointer.removeEventListener(PointerEvent.PRESS, pointerListen);
			pointer.removeEventListener(PointerEvent.RELEASE, pointerListen);
		}
		if (upGraphic != null) {
			upGraphic.destroy();
		}
		if (downGraphic != null) {
			downGraphic.destroy();
		}
	}
	
}