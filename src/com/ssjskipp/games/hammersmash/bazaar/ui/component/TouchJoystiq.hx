package com.ssjskipp.games.hammersmash.bazaar.ui.component;

import com.ssjskipp.games.hammersmash.bazaar.controllers.Pointer;
import com.ssjskipp.games.hammersmash.bazaar.debug.Debug;
import com.ssjskipp.games.hammersmash.bazaar.debug.DebugChannels;
import com.ssjskipp.games.hammersmash.bazaar.events.PointerEvent;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import com.ssjskipp.games.hammersmash.bazaar.maths.Vector;
import nme.display.Shape;
import nme.display.Sprite;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 */
class TouchJoystiq extends Sprite
{
	
	//STYLES
	static inline var MIN_THUMB_DISPLAY:Float = 15;
	static inline var MAX_THUMB_DISPLAY:Float = 25;
	
	public var range(default, setRange):Float;
	public var enabled(default, setEnabled):Bool;
	
	public var directionVector:Vector;
	public var preserveOnRelease:Bool = false;
	
	var pointer:Pointer;
	var rangeDisplay:Shape;
	var thumb:Shape;
	
	var currentTrackedId:Int = -1;
	
	public function new(pointer:Pointer, maximumRange:Float = 100) 
	{
		super();
		
		thumb = new Shape();
		rangeDisplay = new Shape();
		this.pointer = pointer;
		
		range = maximumRange;
		
		enabled = true;
		directionVector = new Vector();
		
		pointer.addEventListener(PointerEvent.PRESS, update);
		pointer.addEventListener(PointerEvent.DRAG, update);
		pointer.addEventListener(PointerEvent.RELEASE, update);
		
		addChild(rangeDisplay);
		addChild(thumb);
	}
	
	public function setRange(newRange:Float):Float {
		range = newRange;
		
		rangeDisplay.graphics.clear();
		rangeDisplay.graphics.beginFill(0xCCCCCC, 0.25);
		rangeDisplay.graphics.drawCircle(0, 0, range);
		rangeDisplay.graphics.endFill();
		
		thumb.graphics.clear();
		thumb.graphics.beginFill(0xFF0000, 0.75);
		thumb.graphics.drawCircle(0, 0, Math.min(Math.max(range / 4, MIN_THUMB_DISPLAY), MAX_THUMB_DISPLAY));
		thumb.graphics.endFill();
		
		return range;
	}
	
	public function setEnabled(isEnabled:Bool):Bool {
		return enabled = isEnabled;
	}
	
	public function update(event:PointerEvent):Void {
		var dX:Float = pointer.x - x;
		var dY:Float = pointer.y - y;
		var d:Float = Math.sqrt(dX * dX + dY * dY);
		var v:Vector = new Vector(dX, dY);
		
		if (currentTrackedId != -1 && currentTrackedId != event.id) return;
		
		if (event.type == PointerEvent.PRESS) {
			//Need a target and in range?
			if (currentTrackedId == -1 && d <= range) {
				currentTrackedId = event.id;
			} else {
				return;
			}
		} else if (event.type == PointerEvent.RELEASE) {
			if (currentTrackedId == -1 || currentTrackedId == event.id) {
				currentTrackedId = -1;
				v.x = 0;
				v.y = 0;
			} else {
				return;
			}
		}
		
		if (currentTrackedId == -1) {
			v.x = 0;
			v.y = 0;
		} else {
			d = Math.min(d, range);
		}
		
		var m:Float = v.getMagnitude();
		if (m > 0) v.scale(d / v.getMagnitude());//Fix distance to a circle
		thumb.x = v.x;
		thumb.y = v.y;
		
		if (preserveOnRelease && currentTrackedId == -1) return;
		directionVector.x = v.x / range;
		directionVector.y = v.y / range;
	}
}