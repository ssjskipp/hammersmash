package com.ssjskipp.games.hammersmash.bazaar.ui;
import com.ssjskipp.games.hammersmash.bazaar.animation.TransitionPlayer;
import com.ssjskipp.games.hammersmash.bazaar.animation.transitions.BaseTransition;
import com.ssjskipp.games.hammersmash.bazaar.animation.transitions.BounceFx;
import com.ssjskipp.games.hammersmash.bazaar.animation.transitions.SlideFx;
import com.ssjskipp.games.hammersmash.bazaar.events.DataEvent;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import com.ssjskipp.games.hammersmash.bazaar.maths.Interpolator;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.bazaar.ui.component.Button;
import com.ssjskipp.games.hammersmash.game.factories.ActorFactory;
import flash.events.Event;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Collection of UIcomponents, events and positioning associated with them.
 */
class Menu implements ITickable {

	public var menuTransitions:TransitionPlayer;
	
	public var enabled:Bool = true;
	
	var contents:List<UIComponent>;
	
	public function new() {
		contents = new List<UIComponent>();
		menuTransitions = new TransitionPlayer();
	}
	
	public function tick(delta:Float, step:Float):Void {
		menuTransitions.tick(delta, step);
	}
	
	function uiEventListen():Void {
		
	}
	
	function addUIComponent(comp:UIComponent):Void {
		contents.add(comp);
	}
	
	function removeUiComponent(comp:UIComponent):Void {
		contents.remove(comp);
	}
	
}