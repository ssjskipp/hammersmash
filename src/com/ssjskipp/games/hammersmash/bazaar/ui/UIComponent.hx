package com.ssjskipp.games.hammersmash.bazaar.ui;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import nme.events.EventDispatcher;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * 
 * a UI Component does UI things and lives inside a menu.
 * 
 * A menu handles UIComponents.
 */
class UIComponent extends EventDispatcher {

	//Graphic for the sprite
	public var graphic:Actor;
	public var enabled:Bool = true;
	public var parent:Menu;
	public var name:String;
	
	public function new(targetGraphic:Actor) {
		super();
		graphic = targetGraphic;
	}
	
	public function destroy():Void {
		
	}
}