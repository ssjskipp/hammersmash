package com.ssjskipp.games.hammersmash.bazaar.ui;

import com.ssjskipp.games.hammersmash.bazaar.ui.component.Button;
import nme.events.Event;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 */
class UIEvent extends Event
{

	public static inline var BUTTON_CLICK:String = "buttonClick";
	public var targetUIComp:UIComponent;
	
	public function new(name:String, buttonClicked:Button) {
		super(name);
		targetUIComp = buttonClicked;
	}
	
}