package com.ssjskipp.games.hammersmash.bazaar.events;
import nme.events.Event;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 */
class PointerEvent extends Event
{
	
	public static inline var PRESS:String = "pointerPress";
	public static inline var RELEASE:String = "pointerRelease";
	public static inline var DRAG:String = "pointerDrag";
	//A 'click' is a short press-release cycle
	public static inline var CLICK:String = "pointerClick";
	
	//id is used to indicate multi-pointer environments
	public var id:Int;

	public function new(type:String, touchId:Int = -1) 
	{
		super(type);
		this.id = touchId;
	}
	
}