package com.ssjskipp.games.hammersmash.bazaar.events;

import nme.events.Event;

/**
 * ...
 * @author Chris Federici
 */

class ButtonEvent extends Event {
	
	public static inline var PRESS:String = "press";
	public static inline var RELEASE:String = "release";
	public static inline var SHORT_PRESS:String = "shortPress";
	
	//The name of the button the event is about. This is left open to any string.
	public var button:String;
	
	public function new(type:String, ?buttonName:String) {
		super(type);
		
		this.button = buttonName;
	}
}