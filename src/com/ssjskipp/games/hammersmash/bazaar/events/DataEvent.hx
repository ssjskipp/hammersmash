package com.ssjskipp.games.hammersmash.bazaar.events;
import nme.events.Event;

/**
 * ...
 * @author Chris Federici
 * 
 * Use the type to handle the data.
 */

class DataEvent extends Event
{

	public var dataValue:Dynamic;
	
	public function new(type:String, ?data:Dynamic, ?bubbles:Bool, ?cancelable:Bool) 
	{
		super(type, bubbles, cancelable);
		
		dataValue = data;
	}
	
}