package com.ssjskipp.games.hammersmash.bazaar.misc;

/**
 * ...
 * @author Chris Federici
 */

interface ITickable  {
	
	//delta - actual time difference in whatever scale
	//step - the scale factor to multiply with. Number of frames stepped
	public function tick(delta:Float, step:Float):Void;
	public var enabled:Bool;
	
}