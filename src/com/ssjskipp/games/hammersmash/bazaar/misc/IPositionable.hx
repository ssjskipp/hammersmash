package com.ssjskipp.games.hammersmash.bazaar.misc;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Something that can be positioned. Has an x, y, setX/getX, setY/getY
 */
interface IPositionable {

	var x(getX, setX):Float;
	var y(getY, setY):Float;
	var radius:Float;
	
}