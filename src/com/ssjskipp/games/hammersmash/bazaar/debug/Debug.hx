package com.ssjskipp.games.hammersmash.bazaar.debug;
import nme.Lib;

/**
 * ...
 * @author A.A. Grapsas
 * 
 * THANKS GRAPSAS
 */

class Debug 
{
	private static var display:DebugOverlay;
	
	public static function initialize():Void
	{
		display = new DebugOverlay();
		#if flash
		#end
	}
	
	public static function log( channel:String, message:String ):Void
	{
		var output:String = "(" + Lib.getTimer() / 1000 + ") " + channel + ": " + message;
		
		trace( output );
		
		if ( display != null )
		{
			display.log( output );
		}
	}
}