package com.ssjskipp.games.hammersmash.bazaar.debug;

/**
 * ...
 * @author A.A. Grapsas
 * 
 * THANKYOU
 */

class DebugChannels 
{
	public static var ENGINE:String = "engine";
	public static var COLLISION:String = "collision";
	public static var DEBUG:String = "debug";
	public static var GAMEPLAY:String = "gameplay";
	public static var ERROR:String = "error";
	public static var WARNING:String = "warning";
	public static var DISPLAY:String = "display";
	public static var SCRIPT:String = "script";
}