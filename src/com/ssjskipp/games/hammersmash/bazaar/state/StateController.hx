package com.ssjskipp.games.hammersmash.bazaar.state;

import nme.events.EventDispatcher;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;

/**
 * ...
 * @author Chris Federici
 * 
 * Controlls different states.
 * States have a name.
 */

class StateController extends EventDispatcher, implements ITickable {

	public var currentStates:Array<State>;
	
	public var enabled:Bool = true;
	
	public function new() {
		super();
		currentStates = new Array<State>();
	}
	
	public function tick(delta:Float, step:Float):Void {
		
		for (state in currentStates) {
			state.tick(delta, step);
		}
		
	}
	
	public function destroy():Void {
		while (currentStates.length > 0) {
			currentStates.pop();
		}
	}
	
	public function changeState(from:String, to:String):Void {
		for (state in currentStates) {
			if (state.name == from) {
				state.exitState();
				state.name = to;
				state.enterState();
				return;
			}
		}
	}
	
	public function hasState(stateName:String):Bool {
		for (i in currentStates) {
			if (i.name == stateName) {
				return true;
			}
		}
		return false;
	}
}