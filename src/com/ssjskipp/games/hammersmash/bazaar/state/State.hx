package com.ssjskipp.games.hammersmash.bazaar.state;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import nme.events.EventDispatcher;

/**
 * ...
 * @author Chris Federici
 * 
 * State is a routine of code that a general type of object can execute.
 * 
 * Has an enter, tick, and exit (code to run on states)
 */

class State extends EventDispatcher, implements ITickable {

	public var id:String = "State";
	
	public var name:String;
	public var target:Dynamic;
	
	public var enabled:Bool = true;
	
	public function new(targetObject:Dynamic = null) {
		super();
		target = targetObject;
	}
	
	//Function to facilitate changing states within the state.
	public function changeState(to:String):Void {
		if (name == to) return;
		exitState();
		name = to;
		enterState();
	}
	
	//Code to run when the state starts
	public function enterState():Void {
		
	}
	
	//Code to run when the state steps
	public function tick(delta:Float, step:Float):Void {
		
	}
	
	//Code to run when the state stops
	public function exitState():Void {
		
	}
	
	override public function toString():String {
		return "[[object State] name = " + name;
	}
	
}