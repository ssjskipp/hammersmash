package com.ssjskipp.games.hammersmash.bazaar.animation.transitions;
import com.ssjskipp.games.hammersmash.bazaar.animation.transitions.BaseTransition;
import com.ssjskipp.games.hammersmash.bazaar.maths.Interpolator;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import nme.display.DisplayObject;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Smooth slide from A to B.
 */
class FadeFx extends BaseTransition {
	
	var startAlpha:Float;
	var deltaAlpha:Float;
	
	var displayObject:DisplayObject;
	
	public function new(duration:Float, target:DisplayObject, endAlpha:Float, ?easeType:String, ?delay:Float) {
		super(duration, null, easeType, delay);
		
		displayObject = target;
		
		startAlpha = target.alpha;
		deltaAlpha = endAlpha - startAlpha;
	}
	
	override public function tick(delta:Float, step:Float):Void {
		if (displayObject == null || !enabled) enabled = false;
		if (enabled){
			t += delta;
			if (t > startTime && t < startTime + duration) {
				displayObject.alpha = Interpolator.Interpolate(t - startTime, startAlpha, deltaAlpha, duration, easeType);
			} else if (t >= startTime + duration) {
				destroy();
			}
		}
	}
	
}