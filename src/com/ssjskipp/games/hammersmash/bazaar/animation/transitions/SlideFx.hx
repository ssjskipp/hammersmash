package com.ssjskipp.games.hammersmash.bazaar.animation.transitions;
import com.ssjskipp.games.hammersmash.bazaar.animation.transitions.BaseTransition;
import com.ssjskipp.games.hammersmash.bazaar.maths.Interpolator;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Smooth slide from A to B.
 */
class SlideFx extends BaseTransition {
	
	var startX:Float;
	var startY:Float;
	var deltaX:Float;
	var deltaY:Float;
	var endX:Float;
	var endY:Float;
	
	public function new(duration:Float, target:Actor, endX:Float, endY:Float, ?easeType:String, ?delay:Float) {
		super(duration, target, easeType, delay);
		
		startX = target.x;
		startY = target.y;
		this.endX = endX;
		this.endY = endY;
		deltaX = endX - startX;
		deltaY = endY - startY;
	}
	
	override public function tick(delta:Float, step:Float):Void {
		if (target == null || !enabled) enabled = false;
		if (enabled){
			t += delta;
			if (t > startTime && t < startTime + duration) {
				target.setPosition(Interpolator.Interpolate(t - startTime, startX, deltaX, duration, easeType), Interpolator.Interpolate(t - startTime, startY, deltaY, duration, easeType));
			} else if (t >= startTime + duration) {
				target.setPosition(endX, endY);
				destroy();
			}
		}
	}
	
}