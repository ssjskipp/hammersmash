package com.ssjskipp.games.hammersmash.bazaar.animation.transitions;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import com.ssjskipp.games.hammersmash.bazaar.maths.Interpolator;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Can also be used as a dummy/delay transition
 */
class BaseTransition implements ITickable {

	public var startTime:Float;
	public var duration:Float;
	public var target:Actor;
	public var easeType:String;
	public var enabled:Bool;
	
	public var name:String;
	
	var t:Float = 0;
	
	public function new(duration:Float, target:Actor, ?easeType:String, ?delay:Float) {
		this.startTime = delay != null ? delay : 0;
		this.easeType = easeType == null ? Interpolator.EASE_LINEAR : easeType;
		this.duration = duration;
		this.target = target;
		enabled = true;
	}
	
	/* Override this and destroy to make new transitions */
	
	public function tick(delta:Float, step:Float):Void {
		if (enabled){
			t += delta;
			if (t >= startTime + duration) {
				destroy();
			}
		}
	}
	
	public function destroy():Void {
		enabled = false;
	}
	
}