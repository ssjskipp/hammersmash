package com.ssjskipp.games.hammersmash.bazaar.animation.transitions;
import com.ssjskipp.games.hammersmash.bazaar.animation.transitions.BaseTransition;
import com.ssjskipp.games.hammersmash.bazaar.maths.Interpolator;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Smooth slide from A to B.
 */
class BounceFx extends BaseTransition {
	
	var startY:Float;
	var deltaY:Float;
	var flip:Bool = false;
	var easeDown:String;
	
	public function new(duration:Float, target:Actor, baseLine:Float, topLine:Float, ?easeTypeUp:String, ?easeTypeDown:String, ?delay:Float) {
		super(duration, target, easeTypeUp, delay);
		
		easeDown = easeTypeDown;
		
		startY = baseLine;
		deltaY = topLine - baseLine;
	}
	
	override public function tick(delta:Float, step:Float):Void {
		if (target == null || !enabled) enabled = false;
		t += delta;
		if (t > startTime && t < startTime + duration) {
			if (t < startTime + duration * 2 / 3) {
				target.setPosition(null, Interpolator.Interpolate(t - startTime, startY, deltaY, duration * 2 / 3, easeType));
			} else {
				if (flip == false) {
					deltaY = -deltaY;
					startY = target.y;
					flip = true;
					easeType = easeDown;
				}
				target.setPosition(null, Interpolator.Interpolate(t - startTime - duration * 2 / 3, startY, deltaY, duration * 1 / 3, easeType));
			}
		} else if (t >= startTime + duration) {
			target.y = startY + deltaY;
			enabled = false;
		}
	}
	
}