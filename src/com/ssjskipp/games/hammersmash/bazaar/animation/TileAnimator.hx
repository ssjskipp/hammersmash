package com.ssjskipp.games.hammersmash.bazaar.animation;
import com.ssjskipp.games.hammersmash.bazaar.assets.SpriteAsset;
import com.ssjskipp.games.hammersmash.bazaar.debug.Debug;
import com.ssjskipp.games.hammersmash.bazaar.debug.DebugChannels;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import com.ssjskipp.games.hammersmash.bazaar.render.TileRenderItem;
import nme.events.Event;
import nme.events.EventDispatcher;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Animates a TileSprite (internal to an Actor usually)
 * 
 * Pass me a tile render item, plox. One associated with an actor, would be great.
 * 
 * How to use:
	 * Make an actor. Use ActorFactory for examples of that.
	 * Create a TileAnimator, passing it an Actor's .sprite and [FRAME_1, FRAME_2, ...] as an array of strings of sprite asset names.
 */
class TileAnimator extends EventDispatcher, implements ITickable {

	//Class = Event, type = ANIMATION_COMPLETE, dispatches when animation has gone from last frame back to first.
	public static inline var ANIMATION_COMPLETE:String = "animationComplete";
	public static inline var DEFAULT_FRAME_RATE:Float = 1 / 30;
	
	public var enabled:Bool = true;
	public var sourceSprite:TileRenderItem = null;
	public var currentFrame(getCurrentFrame, setCurrentFrame):Int;
	public var numFrames:Int;
	public var frameRate:Float = DEFAULT_FRAME_RATE;
	public var loop:Bool = true;
	
	var frames:Array<SpriteAsset>;
	var timeSinceLastUpdate:Float = 0;
	var _currentFrame:Int = 0;
	
	public function new(sourceSprite:TileRenderItem, animationFrames:Array<SpriteAsset>) {
		super();
		
		this.sourceSprite = sourceSprite;
		frames = animationFrames;
		numFrames = frames.length;
	}
	
	public function stop():Void {
		enabled = false;
	}
	
	public function play():Void {
		enabled = true;
	}
	
	public function gotoAndPlay(frame:Int):Void {
		setCurrentFrame(frame);
		play();
	}
	
	public function gotoAndStop(frame:Int):Void {
		setCurrentFrame(frame);
		stop();
	}
	
	public function getCurrentFrame():Int {
		return _currentFrame;
	}
	
	//If you pass the total frames, loop around.
	public function setCurrentFrame(newValue:Int):Int {
		_currentFrame = newValue % numFrames;
		if (newValue != 0 && _currentFrame == 0) {
			if (loop != true) {
				stop();
			}
			dispatchEvent(new Event(ANIMATION_COMPLETE));
		}
		sourceSprite.setAsset(frames[_currentFrame]);
		return _currentFrame;
	}
	
	//Advance forward one frame
	public function nextFrame():Void {
		currentFrame = currentFrame + 1;
	}
	
	public function tick(delta:Float, step:Float):Void {
		if (!enabled) return;
		
		timeSinceLastUpdate += delta;
		if (timeSinceLastUpdate >= frameRate) {
			nextFrame();
			timeSinceLastUpdate = 0;
		}
	}
	
}