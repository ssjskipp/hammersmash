package com.ssjskipp.games.hammersmash.bazaar.animation;
import com.ssjskipp.games.hammersmash.bazaar.animation.transitions.BaseTransition;
import com.ssjskipp.games.hammersmash.bazaar.events.DataEvent;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import nme.events.Event;
import nme.events.EventDispatcher;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Represents a chain of transitions.
 * 
 * Transitions happen in order, unless the TransitionPlayer gets borked with.
 */
class TransitionPlayer extends EventDispatcher, implements ITickable {

	public static inline var TRANSITION_COMPLETE:String = "transitionComplete";
	
	var transitionList:Array<BaseTransition>;
	
	var toRemove:List<BaseTransition>;
	public var enabled:Bool = true;
	
	public function new() {
		super();
		transitionList = new Array<BaseTransition>();
		toRemove = new List<BaseTransition>();
	}
	
	public function playTransition(transition:BaseTransition):Void {
		transitionList.push(transition);
	}
	
	public function tick(delta:Float, step:Float):Void {
		
		if (!enabled) return;
		
		for (transition in transitionList) {
			if (transition == null || !transition.enabled) {
				toRemove.add(transition);
				dispatchEvent(new DataEvent(TRANSITION_COMPLETE, transition));
			} else {
				transition.tick(delta, step);
			}
		}
		
		for (item in toRemove) { transitionList.remove(item); }
		toRemove.clear();
	}
	
}