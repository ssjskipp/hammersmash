package com.ssjskipp.games.hammersmash.bazaar;

import com.ssjskipp.games.hammersmash.bazaar.assets.Asset;
import com.ssjskipp.games.hammersmash.bazaar.assets.SpriteAsset;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import com.ssjskipp.games.hammersmash.bazaar.render.TileRenderer;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.game.factories.ActorFactory;

/**
 * ...
 * @author Chris Federici
 * 
 * Still super coupled with Actor/TileSprite/Physics
 * Need to make them play nicer. Think later on this.
 *       6am is not the time to untie knots.
 * 
 * NEED TO UPDATE TILE SHEET TO BE ABLE TO NOT RENDER THINGS
 * MAKE TILE SHEET BE ABLE TO ACTUALLY REMOVE THINGS AND ADD THEM IN A SENSE MAKING MANOR
 * 
 * Essentially a pool and a ticker.
 */

class ParticleSystem implements ITickable
{
	static inline var DEFAULT_MAX_PARTICLES:Int = 200;
	/* Signiture of a tickable -- Returns: voids, Recieves:
		 * delta (time difference in seconds)
		 * step (time difference as a ratio of frame rate)
		 * array of particles to iterate */
	public var particleFunction:Float -> Float -> ParticleSystem -> Void;
	
	public var freeIndex(default, null):Int;
	public var particles(default, null):Array<Actor>;
	public var numParticles(default, null):Int;
	public var enabled:Bool = true;
	
	public function new(spriteString:String, layer:Int, maxParticles:Int = DEFAULT_MAX_PARTICLES, ?particleFunctionPointer:Float -> Float -> ParticleSystem -> Void) {
		numParticles = maxParticles;
		particles = new Array<Actor>();
		for (i in 0...numParticles) {
			var newActor:Actor = ActorFactory.BuildActor(spriteString, layer);
			newActor.x = -10;
			newActor.y = -10;
			newActor.enableProjectilePhysics();
			newActor.projectilePhysics.setVector(null, 5);
			particles.push(newActor);
		}
		
		particleFunction = defaultParticlefunction;
		
		if (particleFunctionPointer != null) {
			particleFunction = particleFunctionPointer;
		}
	}
	
	/* This particle behavior should be something else. I'm not sure how I want to make different types of particle systems yet. */
	/* Currently, I need 2: 1 for orb particles, and one for this kind of 'scatter' particle */
	public function tick(delta:Float, step:Float):Void {
		if (particleFunction != null) particleFunction(delta, step, this);
	}
	
	//Returns the start index of added particles. -1 on failure. Up to you not to overflow.
	public function addParticles(x:Float, y:Float, numberOfPartcles:Int, ?impactDirection:Float):Int {
		var out:Int = -1;
		if (numberOfPartcles > 0 && freeIndex + numberOfPartcles < numParticles) {
			out = freeIndex;
			for (i in 0...numberOfPartcles) {
				if (freeIndex >= numParticles) break;
				var myParticle:Actor = particles[freeIndex];
				myParticle.x = x;
				myParticle.y = y;
				var resultDirection:Float;
				if (impactDirection != null) {
					resultDirection = Math.random() * Math.PI / 4 * (Math.random() > 0.5 ? 1 : -1) + impactDirection;
				} else {
					resultDirection = Math.random() * Math.PI * 2;
				}
				myParticle.projectilePhysics.setVector(resultDirection, Math.random() * 10 + 3);
				freeIndex++;
			}
		}
		return out;
	}
	
	public function removeParticleAt(index:Int):Void {
		var swap:Actor;
		if (freeIndex > 0) {
			freeIndex--;
			swap = particles[index];
			swap.x = -10;
			swap.y = -10;
			particles[index] = particles[freeIndex];
			particles[freeIndex] = swap;
		}
	}
	
	private function defaultParticlefunction(delta:Float, step:Float, particleSystem:ParticleSystem):Void {
		var n:Int = freeIndex - 1;
		var particles:Array<Actor> = particleSystem.particles;
		while (n >= 0){
			var particle:Actor = particles[n];
			particle.x += particle.projectilePhysics.xSpeed * step;
			particle.y -= particle.projectilePhysics.ySpeed * step;
			var newSpeed = particle.projectilePhysics.speed * 0.95;
			if (Math.abs(newSpeed) < 0.5) {
				removeParticleAt(n);
			} else {
				particle.projectilePhysics.setVector(null, newSpeed);
			}
			n--;
		}
	}
	
}