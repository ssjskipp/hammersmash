package com.ssjskipp.games.hammersmash.bazaar.physics;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import com.ssjskipp.games.hammersmash.bazaar.maths.Vector;

/**
 * ...
 * @author Chris Federici
 * 
 * Useful for anything that wants to have projectile physics.
 * 
 * Any change to the speeds or direction will propegate throughout the others.
 * 
 * EXTEND THIS TO TAKE VELOCITY, POSITION, AND ACCELERATION
 * 
 * ALSO ALSO, THIS SHOULD MOVE TO A PHYSICS MANAGER MAYBE. SO I CAN DO A BATCH TICK INSTEAD OF THIS LOOP BULLSHIT
 * 
 * ALSO, ALSO, ALSO I NEED TO DETERMINE IF THE Y AXIS IS INVERTED
 */
 
class ProjectilePhysics implements ITickable {
	
	public var xSpeed(default, null):Float = 0;
	public var ySpeed(default, null):Float = 0;
	public var direction(default, null):Float = 0;
	public var speed(default, null):Float = 0;
	
	public var position(default, null):Vector;
	public var velocity(default, null):Vector;
	public var acceleration(default, null):Vector;
	
	public var enabled:Bool = true;
	
	public function new() {
		position = new Vector();
		velocity = new Vector();
		acceleration = new Vector();
	}
	
	public function tick(delta:Float, step:Float):Void {
		//Just increment velocity + position
		velocity.x += acceleration.x * step;
		velocity.y += acceleration.y * step;
		position.x += velocity.x * step;
		position.y += velocity.y * step;
	}
	
	public function setVector(direction:Null<Float>, speed:Null<Float>):Void {
		if (direction == null && speed == null) return;
		if (direction != null) this.direction = direction;
		if (speed != null) this.speed = speed;
		xSpeed = this.speed * Math.cos(this.direction);
		ySpeed = this.speed * Math.sin(this.direction);
	}
	
	public function setPoint(x:Null<Float>, y:Null<Float>):Void {
		if (x == null && y == null) return;
		if (x != null) xSpeed = x;
		if (y != null) ySpeed = y;
		direction = Math.atan2(ySpeed, xSpeed);
		speed = Math.sqrt(x * x + y * y);
	}
	
}