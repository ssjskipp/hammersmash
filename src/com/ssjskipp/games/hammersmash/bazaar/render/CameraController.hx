package com.ssjskipp.games.hammersmash.bazaar.render;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import nme.display.DisplayObject;

/**
 * ...
 * @author Chris Federici
 */

class CameraController implements ITickable {

	var camera:Camera;
	public var shakeDistance:Float = 0;
	
	public var enabled:Bool = true;
	
	var saveX:Float;
	var saveY:Float;
	
	public function new(displayTarget:DisplayObject) {
		camera = new Camera(displayTarget);
	}
	
	public function tick(delta:Float, step:Float):Void {
		//Shake the camera
		if (shakeDistance <= 0) {
			shakeDistance = 0;
			saveX = camera.display.x;
			saveY = camera.display.y;
		}
		shakeDistance *= 0.9;
		if (Math.abs(shakeDistance) < 1) shakeDistance = 0;
		var shakeX:Float = shakeDistance * Math.cos(Math.random() * Math.PI * 2);
		var shakeY:Float = shakeDistance * Math.cos(Math.random() * Math.PI * 2);
		camera.display.x = saveX + shakeX;
		camera.display.y = saveY + shakeY;
	}
	
}