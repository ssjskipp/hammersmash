package com.ssjskipp.games.hammersmash.bazaar.render;
import com.ssjskipp.games.hammersmash.bazaar.assets.SpriteAsset;
import com.ssjskipp.games.hammersmash.bazaar.debug.Debug;
import com.ssjskipp.games.hammersmash.bazaar.debug.DebugChannels;
import nme.display.Sprite;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Represents an item in the render array.
 * This class touches the RenderList's render list data.
 * It's TileLayer's responsibility to change this item's index.
 * 
 * The infomation about a render item comes from its internal spriteAsset.
 * 
 * index = the actual index in the renderArray to draw this item.
 * layer = parent layer containing this item.
 * renderArray = reference to the TileRenderer's input array
 */
class TileRenderItem {
	
	public var parentLayer:TileLayer;
	public var renderArray:Array<Float>;
	public var spriteAsset:SpriteAsset;
	
	public var x(getX, setX):Float;
	public var y(getY, setY):Float;
	public function getHeight():Float { return spriteAsset.height; }
	public function getWidth():Float { return spriteAsset.width; }
	public var width(getWidth, null):Float;
	public var height(getHeight, null):Float;
	public var rotation(getRotation, setRotation):Float;
	public var tileID(getTileID, setTileID):Float;
	
	public var index(getIndex, setIndex):Int;
	
	var _x:Float = 0;
	var _y:Float = 0;
	var _id:Float = 0;
	var _rotation:Float = 0;
	var _index:Int;
	
	public function new(renderArray:Array<Float>, asset:SpriteAsset, index:Int) {
		this.renderArray = renderArray;
		this.spriteAsset = asset;
		_index = index;
		tileID = asset.renderId;
	}
	
	//Used for animation. Change the internal data (render ID, width, heigt) to match the given asset.
	public function setAsset(newAsset:SpriteAsset):Void {
		this.spriteAsset = newAsset;
		setTileID(newAsset.renderId);
	}
	
	public function getIndex():Int {
		return _index;
	}
	
	public function setIndex(value:Int):Int {
		_index = value;
		if (value == -1) {
			parentLayer = null;
			renderArray = null;
			spriteAsset = null;
		} else {
			setX(_x);
			setY(_y);
			setRotation(_rotation);
			setTileID(_id);
		}
		return _index;
	}
	
	public function getX():Float {
		if (index == -1) {
			return 0;
		}
		return _x;
	}
	
	public function setX(value:Float):Float {
		if (index == -1) {
			return 0;
		}
		return _x = renderArray[index + TileRenderList.OFFSET_X] = value;
	}
	
	public function getY():Float {
		if (index == -1) {
			return 0;
		}
		return _y;
	}
	
	public function setY(value:Float):Float {
		if (index == -1) {
			return 0;
		}
		return _y = renderArray[index + TileRenderList.OFFSET_Y] = value;
	}
	
	public function getRotation():Float {
		if (index == -1) {
			return 0;
		}
		return _rotation;
	}
	
	public function setRotation(value:Float):Float {
		if (index == -1) {
			return 0;
		}
		return _rotation = renderArray[index + TileRenderList.OFFSET_ROTATION] = value;
	}
	
	public function getTileID():Float {
		if (index == -1) {
			return 0;
		}
		return _id;
	}
	
	public function setTileID(value:Float):Float {
		if (index == -1) {
			return 0;
		}
		return _id = renderArray[index + TileRenderList.OFFSET_TILEID] = value;
	}
	
	public function toString():String {
		return "[Item, index=" + _index + ",x="+_x+",y="+_y+"]";
	}
	
	
}