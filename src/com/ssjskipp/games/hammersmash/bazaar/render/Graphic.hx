package com.ssjskipp.games.hammersmash.bazaar.render;
import com.ssjskipp.games.hammersmash.bazaar.misc.IPositionable;

/**
 * ...
 * @author Chris Federici
 * 
 * Wrapper class for any kind of graphic.
 */

class Graphic implements IPositionable
{
	
	public var x(getX, setX):Float = 0;
	public var y(getY, setY):Float = 0;
	public var radius:Float = 0;
	public var rotation(default, setRotation):Float = 0;
	public var width(default, null):Float = 0;
	public var height(default, null):Float = 0;
	
	public function getX():Float {
		return x;
	}
	
	public function getY():Float {
		return y;
	}
	
	public function setX(newX:Float = 0):Float {
		return x = newX;
	}
	
	public function setY(newY:Float = 0):Float {
		return y = newY;
	}
	
	public function setRotation(newTheta:Float = 0):Float {
		return rotation = newTheta;
	}
	
	public function new(?x:Float, ?y:Float, ?width:Float, ?height:Float) {
		if (x != null) setX(x);
		if (y != null) setY(y);
		if (width != null) this.width = width;
		if (height != null) this.height = height;
		radius = Math.max(this.width, this.height) / 2;
	}
	
}