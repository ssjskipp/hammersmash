package com.ssjskipp.games.hammersmash.bazaar.render;
import com.ssjskipp.games.hammersmash.bazaar.assets.SpriteAsset;
import com.ssjskipp.games.hammersmash.bazaar.debug.Debug;
import com.ssjskipp.games.hammersmash.bazaar.debug.DebugChannels;
import com.ssjskipp.games.hammersmash.bazaar.events.ButtonEvent;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Manages a RenderList.
 * 
 * Keeps track of the objects in the list, and where they exist in relation to parent layers.
 * 
 * Has an internet render list that can be passed up.
 * 
 * It's up to the user to tell the RenderList to remove items.
 * 
 * This is the one that controls its layers. The layers control their items.
 * 
 * Use this to add items. Specify the layer, and if needed index, of the item.
 */
class TileRenderList {
	
	public static inline var ITEM_SIZE:Int = 4;
	public static inline var OFFSET_X:Int = 0;
	public static inline var OFFSET_Y:Int = 1;
	public static inline var OFFSET_TILEID:Int = 2;
	public static inline var OFFSET_ROTATION:Int = 3;

	public var rawList:Array<Float>;
	public var maxLayer:Int;
	
	var renderLayers:Array<TileLayer>;
	
	public function new(numLayers:Int) {
		rawList = new Array<Float>();
		renderLayers = new Array<TileLayer>();
		for (i in 0...numLayers) {
			renderLayers.push(new TileLayer(rawList, i));
			renderLayers[i].layerId = i;
		}
		maxLayer = numLayers;
	}
	
	/* Create a new render item. Use this to add items to a TileRenderer list.
	 * Manipulate the returns TileRenderItem.
	 * * */
	public function addNewRenderItem(asset:SpriteAsset, layer:Int = 0, x:Float = 0, y:Float = 0, rotation:Float = 0):TileRenderItem {
		var newItem:TileRenderItem = null;
		var offset:Int = 0;
		if (layer < maxLayer) {
			for (i in 0...layer) {
				offset += renderLayers[layer].globalOffset;
			}
			offset += renderLayers[layer].children.length * ITEM_SIZE;
			for (i in 0...ITEM_SIZE){
				rawList.insert(offset, 0);
			}
			newItem = renderLayers[layer].addNewItem(rawList, asset);
			newItem.x = x;
			newItem.y = y;
			newItem.rotation = rotation;
			for (i in (layer+1)...maxLayer) {
				renderLayers[i].globalOffset += ITEM_SIZE;
			}
		} else {
			Debug.log(DebugChannels.ERROR, "Layer index (" + layer + ") out of bounds (" + maxLayer + ")");
		}
		return newItem;
	}
	
	public function removeItem(item:TileRenderItem):Void {
		var layerId:Int;
		if (item.index != -1 && item.parentLayer != null) {
			layerId = item.parentLayer.layerId;
			rawList.splice(item.index, ITEM_SIZE);
			item.parentLayer.removeItem(item);
			for (i in (layerId + 1)...maxLayer) {
				renderLayers[i].globalOffset -= ITEM_SIZE;
			}
		}
	}
	
	public function removeItemAt(layer:Int, index:Int):Void {
		var success:Bool = false;
		if (layer < maxLayer) {
			removeItem(renderLayers[layer].children[index]);
		} else {
			Debug.log(DebugChannels.ERROR, "Layer index (" + layer + ") out of bounds (" + maxLayer + ")");
		}
	}
	
}