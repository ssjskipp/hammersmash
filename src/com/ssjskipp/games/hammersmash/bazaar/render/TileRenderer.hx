package com.ssjskipp.games.hammersmash.bazaar.render;
import com.ssjskipp.games.hammersmash.bazaar.assets.Asset;
import com.ssjskipp.games.hammersmash.bazaar.assets.SpriteAsset;
import nme.display.BitmapData;
import nme.display.Graphics;
import nme.display.Tilesheet;
import nme.geom.Point;
//import com.ssjskipp.games.hammersmash.bazaar.maths.Rectangle;
import nme.geom.Rectangle;

/**
 * ...
 * @author Chris Federici
 * 
 * Wrapper for working with a TileSheet as a render source.
 * 
 * Maintains a list of sprites to keep track of and render.
 * 
 * Render process idea: Keep track of every sprite to render with this class, updating as needed.
 */

class TileRenderer extends Tilesheet
{

	public static var REF:TileRenderer;
	public static inline var RENDER_FLAGS:Int = Tilesheet.TILE_ROTATION;
	
	public var isReady(default, null):Bool;
	public var renderList:TileRenderList;
	
	private var rectAssetCount:Int = 0;
	
	public function new(bitmapSource:BitmapData, numLayers:Int = 1) {
		super(bitmapSource);
		
		REF = this;
		
		isReady = true;
		renderList = new TileRenderList(numLayers);
	}
	
	public function addRect(rectangle:Rectangle, centerPoint:Point = null):Int {
		var out:Int = rectAssetCount;
		super.addTileRect(new nme.geom.Rectangle(rectangle.x, rectangle.y, rectangle.width, rectangle.height), centerPoint);
		rectAssetCount++;
		return out;
	}
	
	function intCompare(x:Int, y:Int):Int {
		if (x == y) return 0;
		if (x > y) return 1;
		return -1;
	}
	
	public function postRender(renderTarget:Graphics):Void {
		this.drawTiles(renderTarget, renderList.rawList, false, RENDER_FLAGS);
	}
}