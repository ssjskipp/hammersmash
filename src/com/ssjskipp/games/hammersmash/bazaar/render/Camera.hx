package com.ssjskipp.games.hammersmash.bazaar.render;
import nme.display.DisplayObject;

/**
 * ...
 * @author Chris Federici
 * 
 * A Camera controlls a Sprite (render target, blitted bitmap) by manipulating it on the screen.
 * Only one sprite can be viewed by a camera at a time.
 * 
 * Changing the dimensions (x, y, width, height) of the camera canges the area in screen coordinates that the camera occupies.
 * 	+ Default is max screen size in pixels
 * 
 * Use setPosition to move the inner contents of the sprite
 */

class Camera
{

	public var display:DisplayObject;
	
	public var x(default, setX):Float = 0;
	public var y(default, setY):Float = 0;
	
	public function setX(newValue:Float):Float { return x = newValue; }
	public function setY(newValue:Float):Float { return y = newValue; }
	
	public function new(displayTarget:DisplayObject) 
	{
		display = displayTarget;
	}
	
}