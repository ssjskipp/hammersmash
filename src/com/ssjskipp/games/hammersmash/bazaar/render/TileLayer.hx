package com.ssjskipp.games.hammersmash.bazaar.render;
import com.ssjskipp.games.hammersmash.bazaar.assets.SpriteAsset;
import com.ssjskipp.games.hammersmash.bazaar.debug.Debug;
import com.ssjskipp.games.hammersmash.bazaar.debug.DebugChannels;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * In charge of managing internal RenderList items.
 * 
 */
class TileLayer {
	
	public var globalOffset(default, setOffset):Int = 0;
	public var layerId:Int;
	public var children:Array<TileRenderItem>;
	
	public function new(renderArray:Array<Float>, layerId:Int) {
		children = new Array<TileRenderItem>();
		this.layerId = layerId;
	}
	
	public function addNewItem(renderArray:Array<Float>, asset:SpriteAsset):TileRenderItem {
		var newItem:TileRenderItem = new TileRenderItem(renderArray, asset, globalOffset + children.length * TileRenderList.ITEM_SIZE);
		newItem.parentLayer = this;
		children.push(newItem);
		return newItem;
	}
	
	//Removes an item from this layer
	public function removeItem(item:TileRenderItem):Bool {
		if (children.remove(item)) {
			item.index = -1;
			updateChildren();
			return true;
		}
		return false;
	}
	
	public function removeItemAt(index:Int):Bool {
		var out:TileRenderItem;
		if (index >= 0 && children.length < index) {
			out = children[index];
			return removeItem(out);
		} else {
			return false;
		}
	}
	
	function updateChildren():Void {
		for (i in 0...children.length) {
			children[i].index = globalOffset + i * TileRenderList.ITEM_SIZE;
		}
	}
	
	public function setOffset(newValue:Int):Int {
		globalOffset = newValue;
		updateChildren();
		return newValue;
	}
	
}