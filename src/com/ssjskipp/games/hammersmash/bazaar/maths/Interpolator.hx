package com.ssjskipp.games.hammersmash.bazaar.maths;
import nme.geom.Point;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Static functions to interpolate things.
 * 
 * all values of 't' are expected between 0 and 1
 */

class Interpolator
{
	
	public static inline var EASE_LINEAR:String = "easeLinear";
	public static inline var EASE_QUAD_IN_OUT:String = "easeQuadInOut";
	public static inline var EASE_QUART_IN:String = "easeQuartIn";
	public static inline var EASE_QUART_OUT:String = "easeQuartOut";
	public static inline var EASE_QUART_IN_OUT:String = "easeQuartInOut";
	public static inline var EASE_QUINT_IN:String = "easeQuintIn";
	public static inline var EASE_QUINT_OUT:String = "easeQuintOut";
	public static inline var EASE_CIRC_OUT:String = "easeCircOut";
	public static inline var EASE_CIRC_IN:String = "easeCircIn";
	
	public function new() {
		
	}
	
	//Easing functions liberated from http://www.gizma.com/easing
	public static function Interpolate(currentTime:Float, startValue:Float, changeBy:Float, duration:Float, easeType:String):Float {
		var out:Float = startValue;
		switch (easeType) {
			case EASE_LINEAR:
				out += currentTime * changeBy / duration;
			case EASE_QUAD_IN_OUT:
				currentTime /= duration / 2;
				if (currentTime < 1) {
					out += changeBy / 2 * currentTime * currentTime;
				} else {
					currentTime--;
					out -= changeBy / 2 * (currentTime * (currentTime - 2) - 1);
				}
			case EASE_QUART_IN:
				currentTime /= duration;
				out += changeBy * Math.pow(currentTime, 4);
			case EASE_QUART_OUT:
				currentTime /= duration;
				currentTime--;
				out -= changeBy * (Math.pow(currentTime, 4) - 1);
			case EASE_QUART_IN_OUT:
				currentTime /= duration / 2;
				if (currentTime < 1) {
					out += changeBy / 2 * Math.pow(currentTime, 4);
				} else {
					currentTime -= 2;
					out += changeBy / 2 * (Math.pow(currentTime, 4) + 2);
				}
			case EASE_QUINT_IN:
				currentTime /= duration;
				out += changeBy * Math.pow(currentTime, 5);
			case EASE_QUINT_OUT:
				currentTime /= duration;
				currentTime--;
				out += changeBy * (Math.pow(currentTime, 5) + 1);
			case EASE_CIRC_IN:
				currentTime /= duration;
				out -= changeBy * (Math.sqrt(1 - currentTime * currentTime) - 1);
			case EASE_CIRC_OUT:
				currentTime /= duration;
				currentTime--;
				out += changeBy * Math.sqrt(1 - currentTime * currentTime);
		}
		return out;
	}
	
	public static function Linear(t:Float, startPoint:Point, endPoint:Point):Point {
		var coeffA:Float = (1 - t);
		return new Point(coeffA * startPoint.x + t * endPoint.x, coeffA * startPoint.y + t * endPoint.y);
	}
	
	public static function QuadtraticBezierCurve(t:Float, startPoint:Point, endPoint:Point, controlPoint:Point):Point {
		var diff:Float = 1 - t;
		var coeffA:Float = diff * diff;
		var coeffB:Float = 2 * diff * t;
		var coeffC:Float = t * t;
		return new Point(coeffA * startPoint.x + coeffB * controlPoint.x + coeffC * endPoint.x, coeffA * startPoint.y + coeffB * controlPoint.y + coeffC * endPoint.y);
	}
	
	public static function CubicBezierCurve(t:Float, startPoint:Point, endPoint:Point, controlPointStart:Point, controlPointEnd:Point):Point {
		var diff:Float = 1 - t;
		var coeffA:Float = diff * diff * diff;
		var coeffB:Float = 3 * diff * diff * t;
		var coeffC:Float = 3 * diff * t * t;
		var coeffD:Float = t * t * t;
		var newX:Float = coeffA * startPoint.x + coeffB * controlPointStart.x + coeffC * controlPointEnd.x + coeffD * endPoint.x;
		var newY:Float = coeffA * startPoint.y + coeffB * controlPointStart.y + coeffC * controlPointEnd.x + coeffD * endPoint.y;
		return new Point(newX, newY);
	}
}