package com.ssjskipp.games.hammersmash.bazaar.maths;
import com.ssjskipp.games.hammersmash.bazaar.misc.IPositionable;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * because fuck you NME
 */
class Rectangle implements IPositionable {
	
	public var x(getX, setX):Float;
	public var y(getY, setY):Float;
	
	public var width:Float;
	public var height:Float;
	public var radius:Float;

	public function new(x:Float = 0, y:Float = 0, width:Float = 0, height:Float = 0) 
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		radius = Math.max(width, height) / 2;
	}
	
	public function getX():Float { return x; }
	public function getY():Float { return y; }
	public function setX(newValue:Float):Float { return x = newValue; }
	public function setY(newValue:Float):Float { return y = newValue; }
	
	public inline function left():Float { return x; }
	public inline function right():Float { return x + width; }
	public inline function top():Float { return y; }
	public inline function bottom():Float { return y + height; }
	
	public function clone():Rectangle {
		return new Rectangle(x, y, width, height);
	}
	
	public function contains(testX:Float, testY:Float):Bool {
		return (testX >= left() && testX <= right()) && (testY >= top() && testY <= bottom());
	}
	
	public function intersects(toIntersect:Rectangle):Bool {
		//if (toIntersect == null) return false;
		return !(toIntersect.left() > right() || toIntersect.right() < left() || toIntersect.top() > bottom() || toIntersect.bottom() < top());
	}
	
	public inline function toString():String {
		return "[Rectangle (x=" + x + ",y=" + y + ",width=" + width + ",height=" + height + "]";
	}
}