package com.ssjskipp.games.hammersmash.bazaar.maths;

/**
 * ...
 * @author Chris Federici
 * 
 * Everyone loves a vector class :Db
 * 
 * Represents a vector in R^2
 */

class Vector {

	public var x:Float;
	public var y:Float;
	
	public function new(x:Float = 0, y:Float = 0) {
		this.x = x;
		this.y = y;
	}
	
	public function getTheta():Float {
		return Math.atan2(-y, x);
	}
	
	//Add vector to this vector :: A + B = A
	public function add(vector:Vector):Void {
		x += vector.x;
		y += vector.y;
	}
	
	//Subtract vector form this vector :: A - B = A
	public function subtract(vector:Vector):Void {
		x -= vector.x;
		y -= vector.y;
	}
	
	//Dot product with vector :: return A o B
	public function dot(vector:Vector):Float {
		return x * vector.x + y * vector.y;
	}
	
	//Scale this vector by c :: A = A * c
	public function scale(c:Float):Void {
		x *= c;
		y *= c;
	}
	
	public function getMagnitude():Float {
		return Math.sqrt(x * x + y * y);
	}
	
	public function clone():Vector {
		return new Vector(x, y);
	}
	
	public function toString():String {
		return "[Vector (x=" + x + ",y=" + y + ")]";
	}
	
}