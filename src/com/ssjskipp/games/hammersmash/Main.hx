package com.ssjskipp.games.hammersmash;

import com.ssjskipp.games.hammersmash.bazaar.debug.Debug;
import com.ssjskipp.games.hammersmash.bazaar.debug.DebugChannels;
import com.ssjskipp.games.hammersmash.bazaar.assets.AssetManager;
import com.ssjskipp.games.hammersmash.bazaar.controllers.Pointer;
//import com.ssjskipp.games.hammersmash.bazaar.maths.Rectangle;
import nme.geom.Rectangle;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.game.HammerSmashGame;
import nme.Assets;
import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.FPS;
import nme.display.Shape;
import nme.display.Sprite;
import nme.events.Event;
import nme.events.TimerEvent;
import nme.geom.Point;
import nme.Lib;
import nme.ui.Multitouch;
import nme.ui.MultitouchInputMode;
import nme.utils.Timer;

/**
 * ...
 * @author Chris Federici
 * 
 * Main entry point of entire program.
 */

class Main extends Sprite {
	
	var fps:FPS;
	
	var gameDisplay:Sprite;
	var baseGameInstance:HammerSmashGame;
	var assetManager:AssetManager;
	
	public function new() {
		super();
		#if iphone
			Lib.current.stage.addEventListener(Event.RESIZE, init);
		#else
			addEventListener(Event.ADDED_TO_STAGE, init);
		#end
	}
	
	private function init(e) {
		//This is where I should cerate the asset maanger and load in those assets, as well as init the engine I guess.
		//Then create a game and pass it the loaded assets.
		//So, the game is the specific implimentation and logic of the assets that the engine
		//can parse and use.
		
		assetManager = new AssetManager(AssetManagerType.NME);
		
		//For now those assets are loaded directly in the game class. Once I have that abstraction, I'll pass it in and woot woot
		
		gameDisplay = new Sprite();
		baseGameInstance = new HammerSmashGame(assetManager, gameDisplay, stage);
		
		//This can stay here, as I consider it UI. And for now, program UI is different from game UI
		//fps = new FPS(0, 0, 0xFFFFFF);
		//addChild(fps);
		
		//Debug.initialize();
		Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
		Debug.log(DebugChannels.DEBUG, "Touch Events: " + Multitouch.supportsTouchEvents);
		Debug.log(DebugChannels.DEBUG, "Max touch: " + Multitouch.maxTouchPoints);
		
		addChild(gameDisplay);
		
		baseGameInstance.start();
	}
	
	static public function main() {
		var stage = Lib.current.stage;
		stage.scaleMode = nme.display.StageScaleMode.NO_SCALE;
		stage.align = nme.display.StageAlign.TOP_LEFT;
		
		Lib.current.addChild(new Main());
	}
	
}
