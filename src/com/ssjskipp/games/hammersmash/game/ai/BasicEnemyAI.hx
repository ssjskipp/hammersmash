package com.ssjskipp.games.hammersmash.game.ai;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import com.ssjskipp.games.hammersmash.bazaar.maths.Vector;
import com.ssjskipp.games.hammersmash.bazaar.misc.IPositionable;
import com.ssjskipp.games.hammersmash.bazaar.state.State;
import com.ssjskipp.games.hammersmash.game.actors.Enemy;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Simple and standard AI. Will fill when I decide how to make an enemy work.
 */
class BasicEnemyAI extends State, implements ITickable
{

	public static inline var STATE_ID:String = "BasicEnemyAI";
	
	public static inline var CHASE:String = "basicAIChase";
	public static inline var FLEE:String = "basicAIFlee";
	public static inline var ARC:String = "basicAIArc";
	public static inline var HOLD:String = "basicAIHold";
	
	public var attackTarget:IPositionable;
	public var rangeBuffer:Float = 25;
	
	var arcDirection:Bool = false;
	var decisionDelay:Int = 5;
	
	public function new(targetEnemy:Enemy, ?attackTarget:IPositionable) {
		super(targetEnemy);
		this.attackTarget = attackTarget;
		name = CHASE;
	}
	
	override public function enterState():Void {
		super.enterState();
		
		if (name == ARC) {
			arcDirection = Math.random() > 0.5 ? true : false;
		}
	}
	
	function tryToMoveTo(x:Float, y:Float):Void {
		var targetAsEnemy:Enemy = cast target;
		
		var isValid:Bool = true;
		if (x < HammerSmashGame.BOUNDS.left || x > HammerSmashGame.BOUNDS.right) isValid = false;
		if (isValid) targetAsEnemy.x = x;
		
		isValid = true;
		if (y < HammerSmashGame.BOUNDS.top || y > HammerSmashGame.BOUNDS.bottom) isValid = false;
		if (isValid) targetAsEnemy.y = y;
	}
	
	override public function tick(delta:Float, step:Float):Void {
		//No target. Just kinda' stand there.
		if (attackTarget == null) {
			changeState(HOLD);
			return;
		}
		
		var targetAsEnemy:Enemy = cast target;
		var xd:Float = attackTarget.x - targetAsEnemy.x;
		var yd:Float = attackTarget.y - targetAsEnemy.y;
		var distanceSquare:Float = xd * xd + yd * yd;
		var distance:Float = Math.sqrt(distanceSquare);
		
		if (distance > targetAsEnemy.range - rangeBuffer && distance < targetAsEnemy.engageRange) {
			changeState(CHASE);
		} else if (distance < targetAsEnemy.range - 2 * rangeBuffer) {
			changeState(FLEE);
		} else if (Math.abs(distance - targetAsEnemy.range) <= 2 * rangeBuffer) {
			changeState(ARC);
		} else {
			changeState(HOLD);
		}
		
		var v:Vector = new Vector(xd, yd);
		var m:Float = v.getMagnitude();
		var theta:Float = v.getTheta();
		if (m != 0) v.scale(1 / m);
		
		if (distance <= targetAsEnemy.range + 2 * rangeBuffer) {
			targetAsEnemy.fireWeapon(v);
		}
		
		//Move toward target
		if (name == CHASE) {
			v.scale(targetAsEnemy.speed);
		//Move away from target
		} else if (name == FLEE) {
			v.scale(-targetAsEnemy.speed);
		//Arc around target
		} else if (name == ARC) {
			v.x = targetAsEnemy.speed * Math.cos(theta + Math.PI / 2 * (arcDirection ? 1 : -1));
			v.y = -targetAsEnemy.speed * Math.sin(theta + Math.PI / 2 * (arcDirection ? 1 : -1));
		}
		
		if (name != HOLD) {
			tryToMoveTo(targetAsEnemy.x + v.x, targetAsEnemy.y + v.y);
		}
		
	}
	
}