package com.ssjskipp.games.hammersmash.game.ai.states;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 */
class InRangeAttack
{

	public function new() 
	{
		var xd:Float = attackTarget.x - targetAsEnemy.x;
		var yd:Float = attackTarget.y - targetAsEnemy.y;
		var distanceSquare:Float = xd * xd + yd * yd;
		var distance:Float = Math.sqrt(distanceSquare);
	}
	
}