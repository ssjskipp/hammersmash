package com.ssjskipp.games.hammersmash.game.ai.states;

import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.bazaar.state.State;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 */
class ChaseState extends State
{

	public var attackTarget:Actor;
	
	public function new(targetActor:Actor) 
	{
		super(targetActor);
	}
	
	override public function tick(delta:Float, step:Float):Void 
	{
		var xd:Float = attackTarget.x - targetAsEnemy.x;
		var yd:Float = attackTarget.y - targetAsEnemy.y;
		var v:Vector = new Vector(xd, yd);
		var m:Float = v.getMagnitude();
		var theta:Float = v.getTheta();
		if (m != 0) v.scale(1 / m);
	}
	
}