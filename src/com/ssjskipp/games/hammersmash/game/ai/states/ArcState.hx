package com.ssjskipp.games.hammersmash.game.ai.states;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.bazaar.state.State;
import nme.geom.Point;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Move in a circle around a target point
 */
class ArcState extends State
{

	public var arcAround:Point;
	
	public function new(target:Actor) {
		super(target);
	}
	
	override public function enterState():Void {
		arcDirection = Math.random() > 0.5 ? true : false;
	}
	
	override public function tick(delta:Float, step:Float):Void {
		var xd:Float = attackTarget.x - targetAsEnemy.x;
		var yd:Float = attackTarget.y - targetAsEnemy.y;
	}
	
}