package com.ssjskipp.games.hammersmash.game.controllers;
import com.ssjskipp.games.hammersmash.bazaar.animation.TransitionPlayer;
import com.ssjskipp.games.hammersmash.bazaar.animation.transitions.SlideFx;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import com.ssjskipp.games.hammersmash.bazaar.maths.Interpolator;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.bazaar.text.TextBox;
import com.ssjskipp.games.hammersmash.game.factories.ActorFactory;
import com.ssjskipp.games.hammersmash.game.HammerSmashGame;
import com.ssjskipp.games.hammersmash.game.misc.Score;
import nme.Lib;
import nme.text.TextFormatAlign;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Essentially a singleton. Displays information like score and current combo.
 * 
 * Relevant to only HammerSmash.
 */
class ComboMeter implements ITickable {

	static inline var SCORE_OFFSET_X:Float = -200;
	static inline var SCORE_OFFSET_Y:Float = -18;
	static inline var COMBO_OFFSET_X:Float = -120;
	static inline var COMBO_OFFSET_Y:Float = -18;
	
	public var enabled:Bool = true;
	public var hidden:Bool = false;
	
	public var actor:Actor;
	
	var tf:TextBox;
	var combo:TextBox;
	
	var saveX:Float;
	var saveY:Float;
	
	public function new() {
		actor = ActorFactory.BuildActor(ActorFactory.COMBO_METER_FACE, HammerSmashGame.LAYER_UI);
		actor.x = HammerSmashGame.BOUNDS.centerX;
		actor.y = -actor.height;
		
		tf = new TextBox();
		combo = new TextBox();
		#if flash
			tf.setFormat(TextBox.FONT_ANAHEIM, 25, 0xFF0000, true, TextFormatAlign.RIGHT);
			combo.setFormat(TextBox.FONT_ANAHEIM, 25, 0xFF0000, true, TextFormatAlign.CENTER);
		#else
			tf.setFormat(TextBox.FONT_ANAHEIM, 25, 0xFF0000, true, null, native.text.TextFormatAlign.RIGHT);
			combo.setFormat(TextBox.FONT_ANAHEIM, 25, 0xFF0000, true, null, native.text.TextFormatAlign.CENTER);
		#end
		tf.setDimensions(0, 0, 330, 50);
		combo.setDimensions(0, 0, 40, 50);
		
		//Add my text to the stage itself.
		HammerSmashGame.REF.gameStage.addChild(tf);
		HammerSmashGame.REF.gameStage.addChild(combo);
	}
	
	public function hide():Void {
		saveX = actor.x;
		saveY = actor.y;
		actor.x = -1000;
		actor.y = -1000;
		hidden = true;
		enabled = false;
	}
	
	public function show():Void {
		actor.x = saveX;
		actor.y = saveY;
		hidden = false;
		enabled = true;
	}
	
	public function tick(delta:Float, step:Float):Void {
		tf.x = actor.x + SCORE_OFFSET_X;
		tf.y = actor.y + SCORE_OFFSET_Y;
		var txt:String = Std.string(Score.currentScore);
		tf.setText(txt);
		
		combo.x = actor.x + COMBO_OFFSET_X;
		combo.y = actor.y + SCORE_OFFSET_Y;
		txt = Std.string(Score.currentBoxCombo);
		combo.setText(txt);
	}
	
	public function destroy():Void {
		if (actor != null) actor.destroy();
		tf.parent.removeChild(tf);
		combo.parent.removeChild(combo);
	}
	
}