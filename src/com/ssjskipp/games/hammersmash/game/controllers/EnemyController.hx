package com.ssjskipp.games.hammersmash.game.controllers;

import com.ssjskipp.games.hammersmash.bazaar.collide.quadtree.IQuadTreeItem;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import com.ssjskipp.games.hammersmash.bazaar.scene.SceneManager;
import com.ssjskipp.games.hammersmash.game.actors.Enemy;
import com.ssjskipp.games.hammersmash.game.HammerSmashGame;
import com.ssjskipp.games.hammersmash.game.levels.Level;
import com.ssjskipp.games.hammersmash.game.misc.GameStatistics;
import com.ssjskipp.games.hammersmash.game.misc.Projectile;
import com.ssjskipp.games.hammersmash.game.misc.Score;

/**
 * ...
 * @author Chris Federici
 * 
 */

class EnemyController implements ITickable
{

	public var enemyList:Array<Enemy>;
	
	var player:PlayerController;
	var toRemove:Array<Enemy>;
	var toAdd:Array<Enemy>;
	
	public var enabled:Bool = true;
	
	public var isAllDead(checkDead, null):Bool;
	
	public function new(playerControllerReference:PlayerController) {
		enemyList = new Array<Enemy>();
		toRemove = new Array<Enemy>();
		toAdd = new Array<Enemy>();
		
		player = playerControllerReference;
	}
	
	public function checkDead():Bool {
		var sum:Int = enemyList.length + toAdd.length - toRemove.length;
		return sum <= 0;
	}
	
	public function clearAll(leaveParticles:Bool = false):Void {
		var enemy:Enemy;
		while (enemyList.length > 0) {
			enemy = enemyList.pop();
			if (leaveParticles) Level.REF.particles.addParticles(enemy.x, enemy.y, 5);
			enemy.destroy();
		}
	}
	
	public function addEnemy(enemy:Enemy):Void {
		toAdd.push(enemy);
	}
	
	public function removeEnemy(enemy:Enemy):Void {
		toRemove.push(enemy);
	}
	
	public function tick(delta:Float, step:Float):Void {
		var enemy:Enemy;
		var collideList:List<IQuadTreeItem>;
		var itemAsProjectile:Projectile = null;
		
		while (toAdd.length > 0) {
			enemy = toAdd.pop();
			if (enemy != null) {
				enemyList.push(enemy);
			}
		}
		
		while (toRemove.length > 0) {
			enemy = toRemove.pop();
			if (enemy != null){
				enemyList.remove(enemy);
				enemy.destroy();
			}
		}
		
		//Tick the enemy AI states
		for (enemy in enemyList) {
			enemy.aiStates.tick(delta, step);
			
			collideList = HammerSmashGame.REF.qt.queryRange(enemy.collisionRect);
			for (item in collideList) {
				if (item.itemId == Projectile.ID) {
					var xd:Float = enemy.x - item.x;
					var yd:Float = enemy.y - item.y;
					var d:Float = Math.sqrt(xd * xd + yd * yd);
					if (d <= item.attachedItem.radius + enemy.radius) {
						itemAsProjectile = cast item.attachedActor;
						var hp:Int = cast enemy.properties.get("health");
						hp -= itemAsProjectile.damage;
						SceneManager.REF.camera.shakeDistance += 2;
						if (hp <= 0) {
							destroyEnemy(enemy);
						} else {
							enemy.properties.set("health", hp);
						}
						item.destroy();
					}
				}
			}
		}
	}
	
	public function destroyEnemy(enemy:Enemy):Void {
		GameStatistics.postAddIntStatistic(GameStatistics.STAT_ENEMIES_BROKE_INT, 1);
		GameStatistics.postAddIntStatistic(GameStatistics.STAT_ENEMY_KILL_LEVEL_INT, 1);
		Score.postEnemyKill(enemy);
		Level.REF.particles.addParticles(enemy.x, enemy.y, Math.round(Math.random() * 10 + 15));
		removeEnemy(enemy);
	}
}