package com.ssjskipp.games.hammersmash.game.controllers;
import com.ssjskipp.games.hammersmash.bazaar.collide.CollideBall;
import com.ssjskipp.games.hammersmash.bazaar.collide.CollideRect;
import com.ssjskipp.games.hammersmash.bazaar.collide.CollisionData;
import com.ssjskipp.games.hammersmash.bazaar.collide.ICollidable;
import com.ssjskipp.games.hammersmash.bazaar.collide.quadtree.IQuadTreeItem;
import com.ssjskipp.games.hammersmash.bazaar.collide.quadtree.QuadTree;
import com.ssjskipp.games.hammersmash.bazaar.collide.quadtree.QuadTreeCollideBall;
import com.ssjskipp.games.hammersmash.bazaar.debug.Debug;
import com.ssjskipp.games.hammersmash.bazaar.debug.DebugChannels;
import com.ssjskipp.games.hammersmash.bazaar.events.DataEvent;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import com.ssjskipp.games.hammersmash.bazaar.scene.SceneManager;
import com.ssjskipp.games.hammersmash.game.HammerSmashGame;
import com.ssjskipp.games.hammersmash.game.levels.Level;
import com.ssjskipp.games.hammersmash.game.misc.GameStatistics;
import com.ssjskipp.games.hammersmash.game.misc.Projectile;
import com.ssjskipp.games.hammersmash.game.misc.Score;
import com.ssjskipp.games.hammersmash.game.weapon.Weapon;
import nme.events.EventDispatcher;
import nme.events.Event;
import nme.geom.Point;

/**
 * ...
 * @author Chris Federici
 * 
 * NEEDS TO KNOW ABOUT: Player's weapon
 */

class SmashableBoxController extends EventDispatcher, implements ITickable
{
	
	public static inline var BOX_SMASHED_EVENT:String = "boxSmashed";
	
	public var enabled:Bool = true;
	
	public var smashBoxes:List<Actor>;
	var toAdd:List<Actor>;
	var toRemove:List<Actor>;
	
	var playerController:PlayerController;

	public function new(playerControllerReference:PlayerController) {
		super();
		smashBoxes = new List<Actor>();
		toAdd = new List<Actor>();
		toRemove = new List<Actor>();
		playerController = playerControllerReference;
	}
	
	public function addBox(box:Actor):Void {
		//Am I a terrible person for this?
		box.properties.set("health", 5);
		toAdd.add(box);
	}
	
	public function removeBox(box:Actor):Void {
		toRemove.add(box);
	}
	
	public function clearAll(leaveParticles:Bool = false):Void {
		var actor:Actor;
		while (smashBoxes.length > 0) {
			actor = smashBoxes.pop();
			if (leaveParticles) Level.REF.particles.addParticles(actor.x, actor.y, 5);
			actor.destroy();
		}
	}
	
	//Simple distance collision.
	public function collideWith(x:Float, y:Float, radius:Float) {
		var radiusSquare = radius * radius;
		for (box in smashBoxes) {
			var xd:Float = x - box.x;
			var yd:Float = y - box.y;
			var d:Float = xd * xd + yd * yd;
			if (d <= radiusSquare + box.collisionRect.width * box.collisionRect.height) {
				Level.REF.breakCombo();
				destroyBox(box);
			}
		}
	}
	
	public function tick(delta:Float, step:Float):Void {
		var playerWeapon:Weapon = playerController.weapon;
		var itemAsProjectile:Projectile = null;
		
		for (box in toAdd) { smashBoxes.add(box); }
		toAdd.clear();
		
		var collideList:List<IQuadTreeItem>;
		for (box in smashBoxes) {
			collideList = HammerSmashGame.REF.qt.queryRange(box.collisionRect);
			for (item in collideList) {
				if (item.itemId == Projectile.ID) {
					var xd:Float = box.x - item.x;
					var yd:Float = box.y - item.y;
					var d:Float = Math.sqrt(xd * xd + yd * yd);
					
					if (d <= item.attachedItem.radius + box.radius) {
						itemAsProjectile = cast item.attachedActor;
						var hp:Int = cast box.properties.get("health");
						hp -= itemAsProjectile.damage;
						if (hp <= 0) {
							Score.postBoxBreak(box);
							destroyBox(box);
						} else {
							box.properties.set("health", hp);
						}
						item.destroy();
						break;
					}
				}
			}
		}
		
		for (box in toRemove) { 
			smashBoxes.remove(box);
			box.destroy();
		}
		toRemove.clear();
	}
	
	//Handle box destroy effects. Things that happen to other controllers.
	public function destroyBox(box:Actor):Void {
		SceneManager.REF.camera.shakeDistance += 5;
		GameStatistics.postAddIntStatistic(GameStatistics.STAT_BOXES_BROKE_INT, 1);
		GameStatistics.postAddIntStatistic(GameStatistics.STAT_BOX_KILL_LEVEL_INT, 1);
		Level.REF.particles.addParticles(box.x, box.y, Math.round(Math.random() * 10 + 15));
		removeBox(box);
	}
}