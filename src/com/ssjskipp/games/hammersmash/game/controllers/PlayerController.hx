package com.ssjskipp.games.hammersmash.game.controllers;
import com.ssjskipp.games.hammersmash.bazaar.controllers.ButtonInput;
import com.ssjskipp.games.hammersmash.bazaar.controllers.Pointer;
import com.ssjskipp.games.hammersmash.bazaar.maths.Rectangle;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.game.HammerSmashGame;
import com.ssjskipp.games.hammersmash.game.misc.GameStatistics;
import com.ssjskipp.games.hammersmash.game.weapon.IWeaponHolder;
import com.ssjskipp.games.hammersmash.game.weapon.Weapon;
import nme.geom.Point;

/**
 * ...
 * @author Chris Federici
 * 
 * Represents the main player actor, and associated attachments.
 */

class PlayerController implements ITickable, implements IWeaponHolder
{
	
	static inline var GRAB_DELAY:Float = 0.5;
	
	public var weapon(default, attachWeapon):Weapon;
	
	public var pickupDistance:Float = 1000;
	public var enabled:Bool = true;
	public var actor:Actor;
	var controller:ButtonInput;
	var pointer:Pointer;
	
	var grabDelay:Float = 0;

	public function new(playerActor:Actor, attachedController:ButtonInput, attachedPointer:Pointer) {
		actor = playerActor;
		actor.enableProjectilePhysics();
		actor.projectilePhysics.setVector(0, 1);
		
		controller = attachedController;
		pointer = attachedPointer;
	}
	
	public function attachWeapon(newWeapon:Weapon):Weapon {
		weapon = newWeapon;
		return weapon;
	}
	
	public function collideWith(x:Float, y:Float, ?target:Actor):Void {
		var xD:Float = actor.x - x;
		var yD:Float = actor.y - y;
		var distance:Float = Math.sqrt(xD * xD + yD * yD);
		var compareRadius:Float = actor.radius;
		if (target != null) {
			compareRadius += target.radius;
		}
		if (distance <= compareRadius) {
			enabled = false;
		}
	}
	
	public function killPlayer():Void {
		enabled = false;
		GameStatistics.postAddIntStatistic(GameStatistics.STAT_TIMES_DEAD_INT, 1);
	}
	
	
	/* TODO for future: implement collision given start, destinaion, and collidables. */ 
	public function tryToMoveTo(x:Float, y:Float):Void {
		var isValid:Bool = true;
		if (x < HammerSmashGame.BOUNDS.left || x > HammerSmashGame.BOUNDS.right) isValid = false;
		if (isValid) actor.x = x;
		
		isValid = true;
		if (y < HammerSmashGame.BOUNDS.top || y > HammerSmashGame.BOUNDS.bottom) isValid = false;
		if (isValid) actor.y = y;
	}
	
	public function tick(delta:Float, step:Float):Void {
		var oldX:Float;
		var oldY:Float;
		var newSpeed:Float = actor.projectilePhysics.speed;
		var newMoveTheta:Float = actor.projectilePhysics.direction;
		
		//Recyclable
		var tempX:Float;
		var tempY:Float;
		
		//Jump out early if not enabled.
		if (enabled != true) {
			return;
		}
		
		if (HammerSmashGame.REF.moveStick != null && HammerSmashGame.REF.moveStick.enabled) {
			newMoveTheta = HammerSmashGame.REF.moveStick.directionVector.getTheta();
			newSpeed = 3 * HammerSmashGame.REF.moveStick.directionVector.getMagnitude();
		} else if (controller != null && (controller.isDown(ButtonInput.MOVE_UP) || controller.isDown(ButtonInput.MOVE_DOWN) || controller.isDown(ButtonInput.MOVE_LEFT) || controller.isDown(ButtonInput.MOVE_RIGHT))) {
			
			newSpeed = newSpeed < 3 ? newSpeed + 0.25 * step : 3;
			
			if (controller.isDown(ButtonInput.MOVE_UP)) {
				newMoveTheta = Math.PI / 2;
				if (controller.isDown(ButtonInput.MOVE_LEFT)) {
					newMoveTheta += Math.PI / 4;
				} else if (controller.isDown(ButtonInput.MOVE_RIGHT)) {
					newMoveTheta -= Math.PI / 4;
				}
			} else if (controller.isDown(ButtonInput.MOVE_DOWN)) {
				newMoveTheta = -Math.PI / 2;
				if (controller.isDown(ButtonInput.MOVE_LEFT)) {
					newMoveTheta -= Math.PI / 4;
				} else if (controller.isDown(ButtonInput.MOVE_RIGHT)) {
					newMoveTheta += Math.PI / 4;
				}
			} else {
				if (controller.isDown(ButtonInput.MOVE_LEFT)) {
					newMoveTheta = Math.PI;
				} else if (controller.isDown(ButtonInput.MOVE_RIGHT)) {
					newMoveTheta = 0;
				}
			}
		} else {
			newSpeed = newSpeed > 0 ? newSpeed - 0.5 * step : 0;
		}
		
		//Set new movement vector
		actor.projectilePhysics.setVector(newMoveTheta, newSpeed);
		
		tryToMoveTo(actor.x + actor.projectilePhysics.xSpeed, actor.y - actor.projectilePhysics.ySpeed);
		
		//Set look vectors
		if (HammerSmashGame.REF.turnStick != null && HammerSmashGame.REF.turnStick.enabled) {
			actor.rotation = HammerSmashGame.REF.turnStick.directionVector.getTheta();
		} else {
			tempX = pointer.x - actor.x;
			tempY = pointer.y - actor.y;
			actor.rotation = -Math.atan2(tempY, tempX);
		}
		
		//Update my weapon
		if (weapon != null) {
			weapon.fire();
		}
	}
	
}