package com.ssjskipp.games.hammersmash.game.levels;
import com.ssjskipp.games.hammersmash.bazaar.animation.TileAnimator;
import com.ssjskipp.games.hammersmash.bazaar.animation.TransitionPlayer;
import com.ssjskipp.games.hammersmash.bazaar.animation.transitions.BaseTransition;
import com.ssjskipp.games.hammersmash.bazaar.animation.transitions.SlideFx;
import com.ssjskipp.games.hammersmash.bazaar.controllers.Pointer;
import com.ssjskipp.games.hammersmash.bazaar.events.DataEvent;
import com.ssjskipp.games.hammersmash.bazaar.maths.Rectangle;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import com.ssjskipp.games.hammersmash.bazaar.maths.Interpolator;
import com.ssjskipp.games.hammersmash.bazaar.maths.Vector;
import com.ssjskipp.games.hammersmash.bazaar.ParticleSystem;
import com.ssjskipp.games.hammersmash.bazaar.render.TileRenderItem;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.bazaar.scene.SceneManager;
import com.ssjskipp.games.hammersmash.game.actors.Enemy;
import com.ssjskipp.games.hammersmash.game.ai.BasicEnemyAI;
import com.ssjskipp.games.hammersmash.game.controllers.ComboMeter;
import com.ssjskipp.games.hammersmash.game.controllers.EnemyController;
import com.ssjskipp.games.hammersmash.game.controllers.PlayerController;
import com.ssjskipp.games.hammersmash.game.controllers.SmashableBoxController;
import com.ssjskipp.games.hammersmash.game.factories.ActorFactory;
import com.ssjskipp.games.hammersmash.game.HammerSmashGame;
import com.ssjskipp.games.hammersmash.game.misc.GameStatistics;
import com.ssjskipp.games.hammersmash.game.misc.Projectile;
import com.ssjskipp.games.hammersmash.game.misc.Score;
import com.ssjskipp.games.hammersmash.game.ui.MainMenu;
import com.ssjskipp.games.hammersmash.game.weapon.Grenade;
import com.ssjskipp.games.hammersmash.game.weapon.Weapon;
import nme.events.Event;
import nme.Lib;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Level generates a level. Maybe I'll implement it as a data driven way where I can specify data files to do it. For now though, courage.
 * 
 * Does too much.
 * 
 * I'M SORRY. I'M SO SORRY.
 */

class Level implements ITickable {
	
	public static var REF:Level;
	
	public static inline var PLAYER_START_X:Float = 40;
	public static inline var PLAYER_START_Y:Float = 40;
	
	public static inline var LEVEL_MAIN:Int = 0;
	public static inline var LEVEL_1:Int = 1;
	
	public var enabled:Bool;
	
	//Things for the game to run
	public var comboBreaker:Actor;
	public var comboMeter:ComboMeter;
	public var sceneManager:SceneManager;
	public var particles:ParticleSystem;
	public var enemyController:EnemyController;
	public var smashBoxController:SmashableBoxController;
	public var player:PlayerController;
		var playerGun:Weapon;
	
	public var currentLevel:Int;
	
	public var transitionPlayer:TransitionPlayer;
	
	//Some constants
	static inline var LEVEL_TRANSITION_DURATION:Float = 0.5;
	static inline var LEVEL_START_DELAY:Float = 3;
	
	//I hate myself
	static inline var LEVEL_START_SLIDE_NAME:String = "levelStartIndicator";
	static inline var LEVEL_OUT_SLIDE_NAME:String = "levelStartIndicator";
	static inline var ACTOR_FLY_IN:String = "actorFlyIn";
	static inline var COMBO_FLY_IN:String = "comboFlyIn";
	
	static inline var COMBO_METER_Y:Float = 50;
	
	public var projectiles:Array<Projectile>;
	public var grenades:Array<Grenade>;
	
	var levelOneBoxes:Array<Actor>;
	var lastActionStamp:Float = 0;
	
	var levelStartIndicatorAnimation:TileAnimator;
	var levelStartIndicator:Actor;
	
	var levelStarted:Bool;
	var isGameOver:Bool;
	var isAllDead:Bool;
	
	var tickables:Array<ITickable>;
	
	public function new(scene:SceneManager) {
		sceneManager = scene;
		levelStarted = false;
		enabled = true;
		REF = this;
		
		tickables = new Array<ITickable>();
		
		grenades = new Array<Grenade>();
		projectiles = new Array<Projectile>();
		particles = new ParticleSystem(ActorFactory.PIXEL_SPRITE_W, HammerSmashGame.LAYER_PARTICLES);
		var playerActor:Actor = ActorFactory.BuildActor(ActorFactory.BOX_SPRITE, HammerSmashGame.LAYER_PLAYER, ActorFactory.TYPE_ACTOR);
		player = new PlayerController(playerActor, HammerSmashGame.REF.keyboard, HammerSmashGame.REF.mouse);
		playerActor.setPosition( -50, -50);
		smashBoxController = new SmashableBoxController(player);
		enemyController = new EnemyController(player);
		
		levelOneBoxes = new Array<Actor>();
		transitionPlayer = new TransitionPlayer();
		comboMeter = new ComboMeter();
		comboMeter.hide();
		transitionPlayer.addEventListener(TransitionPlayer.TRANSITION_COMPLETE, levelTransitionEvent);
		
		levelStartIndicator = ActorFactory.BuildActor(ActorFactory.NUM_THREE, HammerSmashGame.LAYER_UI);
		levelStartIndicator.x = HammerSmashGame.BOUNDS.screenToGameX(-levelStartIndicator.width);
		levelStartIndicator.y = HammerSmashGame.BOUNDS.centerY;
		levelStartIndicatorAnimation = new TileAnimator(levelStartIndicator.sprite, ActorFactory.AssetList([ActorFactory.NUM_THREE, ActorFactory.NUM_TWO, ActorFactory.NUM_ONE]));
		levelStartIndicatorAnimation.addEventListener(TileAnimator.ANIMATION_COMPLETE, animationEnd);
		levelStartIndicatorAnimation.stop();
		levelStartIndicatorAnimation.frameRate = 0.75;
		levelStartIndicator.setPosition( -50);
		
		comboBreaker = ActorFactory.BuildActor(ActorFactory.COMBO_BREAKER, HammerSmashGame.LAYER_UI);
		comboBreaker.setPosition(HammerSmashGame.BOUNDS.screenToGameX(-comboBreaker.width));
		
		sceneManager.addTickable(particles);
		sceneManager.addTickable(smashBoxController);
		
		tickables.push(transitionPlayer);
		tickables.push(comboMeter);
		tickables.push(levelStartIndicatorAnimation);
	}
	
	var lastComboBreak:Int = 0;
	public function breakCombo():Void {
		if (!Score.postBreakCombo()) return;
		var currentTime:Int = Lib.getTimer();
		if (currentTime - lastComboBreak < 2500) return;
		SceneManager.REF.camera.shakeDistance += 5;
		lastComboBreak = currentTime;
		comboBreaker.setPosition(HammerSmashGame.BOUNDS.screenToGameX(-comboBreaker.width), HammerSmashGame.BOUNDS.centerY);
		var inTransition:SlideFx = new SlideFx(1, comboBreaker, HammerSmashGame.BOUNDS.centerX, HammerSmashGame.BOUNDS.centerY, Interpolator.EASE_QUART_IN);
		comboBreaker.setPosition(HammerSmashGame.BOUNDS.centerX, HammerSmashGame.BOUNDS.centerY);
		var outTransition:SlideFx = new SlideFx(1, comboBreaker, HammerSmashGame.BOUNDS.worldWidth + comboBreaker.width, HammerSmashGame.BOUNDS.centerY, Interpolator.EASE_QUART_IN, 1.5);
		comboBreaker.setPosition(HammerSmashGame.BOUNDS.screenToGameX(-comboBreaker.width), HammerSmashGame.BOUNDS.centerY);
		transitionPlayer.playTransition(inTransition);
		transitionPlayer.playTransition(outTransition);
	}
	
	//Removes previous level, and clears running data
	public function clearLevel():Void {
		var actor:Actor;
		var grenade:Grenade;
		var projectile:Projectile;
		
		while (grenades.length > 0) {
			grenade = grenades.pop();
			if (grenade.enabled){
				grenade.destroy();
			}
		}
		
		while (projectiles.length > 0) {
			projectile = projectiles.pop();
			if (projectile.enabled) {
				projectile.destroy();
			}
		}
		
		//Menu screen boxes are "special"
		while (levelOneBoxes.length > 0) {
			actor = levelOneBoxes.pop();
			actor.destroy();
		}
		
		comboMeter.hide();
		smashBoxController.clearAll();
		enemyController.clearAll();
		
		sceneManager.removeTickable(player);
		sceneManager.removeTickable(enemyController);
	}
	
	public function GenerateLevel(levelID:Int):Void {
		clearLevel();
		
		enabled = true;
		isGameOver = false;
		levelStarted = false;
		currentLevel = levelID;
		
		//Reset some stats
		GameStatistics.setStatistic(GameStatistics.STAT_BOX_KILL_LEVEL_INT, 0);
		GameStatistics.setStatistic(GameStatistics.STAT_ENEMY_KILL_LEVEL_INT, 0);
		
		if (levelID == LEVEL_MAIN) {
			actualLevelNumber = 0;
			levelZero();
		} else if (levelID == LEVEL_1) {
			actualLevelNumber++;
			Score.resetScore();
			levelOne();
		}
		
		//If it's not the main menu, slide in the relevant elements (combo box, time to start, etc.)
		if (levelID != LEVEL_MAIN) {
			comboMeter.show();
			comboMeter.actor.x = HammerSmashGame.BOUNDS.centerX;
			comboMeter.actor.y = -comboMeter.actor.height;
			levelStartIndicator.setPosition(HammerSmashGame.BOUNDS.screenToGameX(-levelStartIndicator.width));
			var comboMeterSlide:SlideFx = new SlideFx(LEVEL_TRANSITION_DURATION, comboMeter.actor, HammerSmashGame.BOUNDS.centerX, COMBO_METER_Y, Interpolator.EASE_QUART_OUT);
			var levelStartSlide:SlideFx = new SlideFx(LEVEL_TRANSITION_DURATION, levelStartIndicator, HammerSmashGame.BOUNDS.centerX, HammerSmashGame.BOUNDS.centerY, Interpolator.EASE_QUART_OUT);
			levelStartIndicatorAnimation.gotoAndStop(0);
			levelStartSlide.name = LEVEL_START_SLIDE_NAME;
			transitionPlayer.playTransition(comboMeterSlide);
			transitionPlayer.playTransition(levelStartSlide);
		}
		
	}
	
	function levelZero():Void {
		var actor:Actor;
		var xPos:Float;
		var yPos:Float;
		
		var hashData:Hash<String> = new Hash<String>();
		hashData.set(ActorFactory.PROP_USE_QUAD_TREE, "true");
		
		makeBoxes(50);
	}
	
	//These can move somewhere. For now, they describe levels.
	var actualLevelNumber:Int;
	function levelOne():Void {
		//Level formula:
		var numToMake:Int = Std.int(Math.min(10 + actualLevelNumber * 3, 50));
		makeBoxes(numToMake);
		numToMake = Std.int(Math.min(2 + Math.floor(actualLevelNumber / 2), 15));
		makeEnemies(numToMake);
		
		player.enabled = true;
		player.actor.setPosition(PLAYER_START_X, PLAYER_START_Y);
		
		playerGun = new Weapon(player.actor);
		player.weapon = playerGun;
	}
	
	public function animationEnd(event:Event):Void {
		if (event.type == TileAnimator.ANIMATION_COMPLETE && event.target == levelStartIndicatorAnimation && !levelStarted) {
			sceneManager.addTickable(enemyController);
			sceneManager.addTickable(player);
			//Slide that shit out
			var levelStartSlide:SlideFx = new SlideFx(LEVEL_TRANSITION_DURATION, levelStartIndicator, HammerSmashGame.BOUNDS.worldWidth + 25, HammerSmashGame.BOUNDS.centerY, Interpolator.EASE_QUART_OUT);
			levelStartIndicatorAnimation.gotoAndStop(2);
			levelStartSlide.name = LEVEL_OUT_SLIDE_NAME;
			transitionPlayer.playTransition(levelStartSlide);
			
			levelStarted = true;
		}
	}
	
	public function levelTransitionEvent(event:DataEvent):Void {
		var transitionEvent:BaseTransition = cast event.dataValue;
		if (transitionEvent.name == LEVEL_START_SLIDE_NAME) {
			levelStartIndicatorAnimation.gotoAndPlay(0);
		} else if (transitionEvent.name == LEVEL_OUT_SLIDE_NAME) {
			levelStartIndicator.setPosition( -levelStartIndicator.width);
		}
	}
	
	//Helper function for positioning enemies and boxes on main menu and level
	public function setValidRandomPosition(hashMap:Hash<String>):Void {
		var tmpX:Float;
		var tmpY:Float;
		var validWidth:Float = HammerSmashGame.BOUNDS.width;
		var validHeight:Float = HammerSmashGame.BOUNDS.height;
		var isValid:Bool = true;
		var doNotWorry:Int = 10;
		var noSpawnBox:Rectangle = new Rectangle();
		do {
			isValid = true;
			tmpX = HammerSmashGame.BOUNDS.left + Math.random() * validWidth;
			tmpY = HammerSmashGame.BOUNDS.top + Math.random() * validHeight;
			if (currentLevel == LEVEL_MAIN) {
				//Need to check final, not initial.
				noSpawnBox.x = HammerSmashGame.BOUNDS.centerX - HammerSmashGame.REF.gameMenus.title.width / 2;
				noSpawnBox.y = MainMenu.TITLE_Y - HammerSmashGame.REF.gameMenus.title.height / 2;
				noSpawnBox.width = HammerSmashGame.REF.gameMenus.title.width;
				noSpawnBox.height = HammerSmashGame.REF.gameMenus.title.height;
				if (noSpawnBox.contains(tmpX, tmpY)) isValid = false;
				
				noSpawnBox.x = HammerSmashGame.BOUNDS.centerX - HammerSmashGame.REF.gameMenus.playGfx.width / 2;
				noSpawnBox.y = MainMenu.PLAY_Y - HammerSmashGame.REF.gameMenus.playGfx.height / 2;
				noSpawnBox.width = HammerSmashGame.REF.gameMenus.playGfx.width;
				noSpawnBox.height = HammerSmashGame.REF.gameMenus.playGfx.height;
				if (noSpawnBox.contains(tmpX, tmpY)) isValid = false;
			} else {
				var xd:Float = player.actor.x - tmpX;
				var yd:Float = player.actor.y - tmpY;
				var d:Float = Math.sqrt(xd * xd + yd * yd);
				if (d < 250) isValid = false;
				
				noSpawnBox.x = HammerSmashGame.BOUNDS.centerX - comboMeter.actor.width / 2;
				noSpawnBox.y = COMBO_METER_Y - comboMeter.actor.height / 2;
				noSpawnBox.width = comboMeter.actor.width;
				noSpawnBox.height = comboMeter.actor.height;
				if (noSpawnBox.contains(tmpX, tmpY)) isValid = false;
				#if android
					xd = HammerSmashGame.REF.moveStick.x - tmpX;
					yd = HammerSmashGame.REF.moveStick.y - tmpY;
					d = Math.sqrt(xd * xd + yd * yd);
					if (d < HammerSmashGame.REF.moveStick.range) isValid = false;
					xd = HammerSmashGame.REF.turnStick.x - tmpX;
					yd = HammerSmashGame.REF.turnStick.y - tmpY;
					d = Math.sqrt(xd * xd + yd * yd);
					if (d < HammerSmashGame.REF.turnStick.range) isValid = false;
				#end
			}
			doNotWorry--;
		} while (!isValid && doNotWorry > 0);
		hashMap.set(ActorFactory.PROP_X, Std.string(tmpX));
		hashMap.set(ActorFactory.PROP_Y, Std.string(tmpY));
	}
	
	public function makeBoxes(numberToMake:Int = 1):Void {
		var xPos:Float;
		var yPos:Float;
		var actor:Actor;
		var hashData:Hash<String> = new Hash<String>();
		hashData.set(ActorFactory.PROP_USE_QUAD_TREE, "true");
		for (i in 0...numberToMake) {
			
			setValidRandomPosition(hashData);
			actor = ActorFactory.BuildActor(ActorFactory.SMASHBOX_SPRITE, HammerSmashGame.LAYER_0, ActorFactory.TYPE_ACTOR, hashData);
			
			flyActorIn(actor);
			
			actor.properties.set("health", 5);
			smashBoxController.addBox(actor);
		}
	}
	
	public function makeEnemies(numberToMake:Int = 1):Void {
		var xPos:Float;
		var yPos:Float;
		var enemy:Enemy;
		var hashData:Hash<String> = new Hash<String>();
		hashData.set(ActorFactory.PROP_USE_QUAD_TREE, "true");
		for (i in 0...numberToMake) {
			
			setValidRandomPosition(hashData);
			enemy = cast ActorFactory.BuildActor(ActorFactory.ENEMY_SPRITE, HammerSmashGame.LAYER_ENEMY, ActorFactory.TYPE_ENEMY, hashData);
			
			flyActorIn(enemy);
			
			enemy.properties.set("health", 10);
			enemy.aiStates.currentStates.push(new BasicEnemyAI(enemy, player.actor));
			enemyController.addEnemy(enemy);
		}
	}
	
	//Causes an actor at its x and y to fly TO there from a random point on the edge of the screen.
	function flyActorIn(actor:Actor):Void {
		var theta:Float = Math.random() * Math.PI * 4;
		var xPos:Float = (25 + HammerSmashGame.BOUNDS.worldWidth) * Math.cos(theta);
		var yPos:Float = -(25 + HammerSmashGame.BOUNDS.worldHeight) * Math.sin(theta);
		var destinationX:Float = actor.x;
		var destinationY:Float = actor.y;
		actor.setPosition(xPos, yPos);
		var inTransition:SlideFx = new SlideFx(LEVEL_TRANSITION_DURATION, actor, destinationX, destinationY, Interpolator.EASE_QUART_OUT);
		inTransition.name = ACTOR_FLY_IN;
		transitionPlayer.playTransition(inTransition);
	}
	
	public function explodeAllBoxes():Void {
		var particlesPer:Int = Math.floor(Math.max(1, smashBoxController.smashBoxes.length / 100));
		enabled = false;
		while (smashBoxController.smashBoxes.length > 0) {
			var box:Actor = smashBoxController.smashBoxes.pop();
			particles.addParticles(box.x, box.y, particlesPer);
			box.destroy();
		}
	}
	
	/* INTERFACE com.ssjskipp.games.hammersmash.bazaar.misc.ITickable */
	public function tick(delta:Float, step:Float):Void {
		var newBox:Actor;
		var xPos:Float;
		var yPos:Float;
		var comboMeterSlide:SlideFx;
		
		for (tock in tickables) {
			tock.tick(delta, step);
		}
		
		if (!enabled) return;
		
		if (currentLevel == LEVEL_MAIN) {
			if (smashBoxController.smashBoxes.length < 50) {
				makeBoxes(1);
			}
			
			if (HammerSmashGame.REF.mouse.state == PointerState.Up) {
				lastActionStamp = 0;
			}
			
			if (HammerSmashGame.REF.mouse.state == PointerState.Down && lastActionStamp <= 0) {
				//THIS CODE NEEDS TO GO TO A FACTORY.
				newBox = ActorFactory.BuildActor(ActorFactory.GRENADE_SPRITE, HammerSmashGame.LAYER_PARTICLES);
				var newGrenade:Grenade = new Grenade(newBox, new Vector(0, 0), 0);
				newGrenade.actor.projectilePhysics.position.x = HammerSmashGame.REF.mouse.x;
				newGrenade.actor.projectilePhysics.position.y = HammerSmashGame.REF.mouse.y;
				sceneManager.addTickable(newGrenade);
				grenades.push(newGrenade);
				lastActionStamp = 0.25;
			} else {
				lastActionStamp -= delta;
			}
		} else {
			if (levelStarted) {
				if (!isGameOver && enemyController.isAllDead) {
					player.enabled = false;
					isGameOver = true;
					
					player.actor.setPosition( -50, -50);
					
					clearLevel();
					
					//Slide shit out
					comboMeterSlide = new SlideFx(LEVEL_TRANSITION_DURATION, comboMeter.actor, HammerSmashGame.BOUNDS.centerX, -comboMeter.actor.height, Interpolator.EASE_QUART_OUT);
					transitionPlayer.playTransition(comboMeterSlide);
					
					//Show that level over
					HammerSmashGame.REF.gameMenus.loadLevelOverMenu();
				} else if (player.enabled == false && !isGameOver) {
					isGameOver = true;
					actualLevelNumber = 0;
					
					//YOU DIED
					particles.addParticles(player.actor.x, player.actor.y, 25);
					player.actor.setPosition( -50, -50);
					
					SceneManager.REF.camera.shakeDistance += 15;
					
					//CLEAN UP TIME
					clearLevel();
					
					comboMeterSlide = new SlideFx(LEVEL_TRANSITION_DURATION, comboMeter.actor, HammerSmashGame.BOUNDS.centerX, -comboMeter.actor.height, Interpolator.EASE_QUART_OUT);
					transitionPlayer.playTransition(comboMeterSlide);
					
					//Show that GAME OVER
					HammerSmashGame.REF.gameMenus.loadGameOverMenu();
				}
			}
		}
	}
}