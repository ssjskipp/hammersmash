package com.ssjskipp.games.hammersmash.game;

import com.ssjskipp.games.hammersmash.bazaar.assets.BitmapAsset;
import com.ssjskipp.games.hammersmash.bazaar.collide.CollideBall;
import com.ssjskipp.games.hammersmash.bazaar.collide.quadtree.QuadTree;
import com.ssjskipp.games.hammersmash.bazaar.collide.quadtree.QuadTreeCollideBall;
import com.ssjskipp.games.hammersmash.bazaar.debug.Debug;
import com.ssjskipp.games.hammersmash.bazaar.debug.DebugChannels;
import com.ssjskipp.games.hammersmash.bazaar.ParticleSystem;
import com.ssjskipp.games.hammersmash.bazaar.render.TileRenderer;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.bazaar.assets.Asset;
import com.ssjskipp.games.hammersmash.bazaar.assets.SpriteAsset;
import com.ssjskipp.games.hammersmash.bazaar.assets.AssetManager;
import com.ssjskipp.games.hammersmash.bazaar.controllers.ButtonInput;
import com.ssjskipp.games.hammersmash.bazaar.controllers.Pointer;
import com.ssjskipp.games.hammersmash.bazaar.scene.GameBounds;
import com.ssjskipp.games.hammersmash.bazaar.scene.SceneManager;
import com.ssjskipp.games.hammersmash.bazaar.ui.component.TouchJoystiq;
import com.ssjskipp.games.hammersmash.bazaar.ui.Menu;
import com.ssjskipp.games.hammersmash.game.actors.Enemy;
import com.ssjskipp.games.hammersmash.game.ai.BasicEnemyAI;
import com.ssjskipp.games.hammersmash.game.factories.ActorFactory;
import com.ssjskipp.games.hammersmash.game.levels.Level;
import com.ssjskipp.games.hammersmash.game.misc.GameStatistics;
import com.ssjskipp.games.hammersmash.game.ui.MainMenu;
import com.ssjskipp.games.hammersmash.game.weapon.Weapon;
import haxe.Json;
import nme.Assets;
import nme.display.BitmapData;
import nme.display.Shape;
import nme.display.Sprite;
import nme.display.Stage;
import nme.geom.Matrix;
import nme.geom.Point;
import nme.geom.Rectangle;
//import com.ssjskipp.games.hammersmash.bazaar.maths.Rectangle;
import nme.Lib;

/**
 * ...
 * @author Chris Federici
 * 
 * Press start.
 * 
 * HAS THIS STUFF:
	 * SceneManager with which I handle adding/removing lists before/after update.
	 * Level, which generates and manages levels
	 * GameMenu, which is the overlay UI elemnts.
	 * Pointer and ButtonInput, the game inputs
	 * TileRenderer, the rendering of the game
 * 
 * Also contains information about the game/screen/device
 */

class HammerSmashGame {
	
	public static var REF:HammerSmashGame = null;
	public static var BOUNDS:GameBounds;
	
	//Desired game dimensions
	public static inline var WIDTH:Float = 800;
	public static inline var HEIGHT:Float = 480;
	
	var assetManager:AssetManager;
	
	//THINGS FOR RENDERING STUFF
	public var gameStage:Sprite;
	public var gameApi:Shape;
	public var background:Shape;
	public var sceneManager:SceneManager;
	public var spriteAssets:Hash<SpriteAsset>;
	public var render:TileRenderer;
	var spriteSheet:BitmapAsset;
	
	var gameStartTime:Float;
	
	//COLLISION STUFF
	public var qt:QuadTree;
	
	//RENDER LAYERS
	public static inline var LAYER_0:Int = 0;
	public static inline var LAYER_PARTICLES:Int = 1;
	public static inline var LAYER_ENEMY:Int = 2;
	public static inline var LAYER_PLAYER:Int = 3;
	public static inline var LAYER_UI:Int = 4;
	
	/* GAME INPUTS */
	public var keyboard:ButtonInput;
	public var mouse:Pointer;
	//FINE.
	public var moveStick:TouchJoystiq;
	public var turnStick:TouchJoystiq;

	//UI STUFF
	public var gameMenus:MainMenu;
	
	//THINGS TO MAKE LEVELS
	public var Levelerator:Level;
	
	public function new(assets:AssetManager, renderTarget:Sprite, stage:Stage) {
		
		gameStage = renderTarget;
		
		BOUNDS = new GameBounds(WIDTH, HEIGHT, stage.stageWidth, stage.stageHeight);
		
		Debug.log(DebugChannels.DISPLAY, BOUNDS.toString());
		
		//Save game data
		GameStatistics.init();
		
		assetManager = assets;
		
		spriteSheet = assets.loadBitmapAsset("spriteSheet", "img/spriteSheet.png");
		render = new TileRenderer(spriteSheet.bitmapData, 5);
		
		loadAssets();//REQUIRES render TO EXIST. ALSO assetManager.
		
		var gameRenderTarget:Shape = new Shape();
		gameApi = new Shape();
		
		//For now, build the background
		background = new Shape();
		
		//Floor
		var bgAsset:SpriteAsset = spriteAssets.get(ActorFactory.FLOOR_TILE);
		var spriteRect:Rectangle = bgAsset.spriteRect;
		var bg:BitmapData = new BitmapData(Math.ceil(spriteRect.width), Math.ceil(spriteRect.height));
		bg.copyPixels(spriteSheet.bitmapData, spriteRect, new Point(0, 0));
		background.graphics.beginBitmapFill(bg);
		background.graphics.drawRect(0, 0, BOUNDS.worldWidth, BOUNDS.worldHeight);
		background.graphics.endFill();
		
		//Walls
		var matrix:Matrix = new Matrix();
		bgAsset = spriteAssets.get(ActorFactory.WALL_SPRITE);
		spriteRect = bgAsset.spriteRect;
		bg = new BitmapData(Math.ceil(spriteRect.width), Math.ceil(spriteRect.height));
		bg.copyPixels(spriteSheet.bitmapData, spriteRect, new Point(0, 0));
		background.graphics.beginBitmapFill(bg, matrix);
		background.graphics.drawRect(BOUNDS.worldWidth - spriteRect.width, 0, spriteRect.width, BOUNDS.worldHeight);
		background.graphics.endFill();
		matrix.rotate(Math.PI / 2);
		background.graphics.beginBitmapFill(bg, matrix);
		background.graphics.drawRect(0, BOUNDS.worldHeight - spriteRect.height, BOUNDS.worldWidth, spriteRect.height);
		background.graphics.endFill();
		matrix.rotate(Math.PI / 2);
		background.graphics.beginBitmapFill(bg, matrix);
		background.graphics.drawRect(0, 0, spriteRect.width, BOUNDS.worldHeight);
		background.graphics.endFill();
		matrix.rotate(Math.PI / 2);
		background.graphics.beginBitmapFill(bg, matrix);
		background.graphics.drawRect(0, 0, BOUNDS.worldWidth, spriteRect.height);
		background.graphics.endFill();
		
		gameStage.addChild(background);
		gameStage.addChild(gameRenderTarget);
		gameStage.addChild(gameApi);
		
		sceneManager = new SceneManager(stage, gameStage, gameRenderTarget.graphics, render);
		
		//Corners
		bgAsset = spriteAssets.get(ActorFactory.WALL_CORNER);
		spriteRect = bgAsset.spriteRect;
		bg = new BitmapData(Math.ceil(spriteRect.width), Math.ceil(spriteRect.height));
		bg.copyPixels(spriteSheet.bitmapData, spriteRect, new Point(0, 0));
		background.graphics.beginBitmapFill(bg, matrix);
		background.graphics.drawRect(0, 0, spriteRect.width, spriteRect.height);
		background.graphics.endFill();
		matrix.rotate(Math.PI / 2);
		background.graphics.beginBitmapFill(bg, matrix);
		background.graphics.drawRect(WIDTH-spriteRect.width, 0, spriteRect.width, spriteRect.height);
		background.graphics.endFill();
		matrix.rotate(Math.PI / 2);
		background.graphics.beginBitmapFill(bg, matrix);
		background.graphics.drawRect(WIDTH-spriteRect.width, HEIGHT-spriteRect.height, spriteRect.width, spriteRect.height);
		background.graphics.endFill();
		matrix.rotate(Math.PI / 2);
		background.graphics.beginBitmapFill(bg, matrix);
		background.graphics.drawRect(0, HEIGHT-spriteRect.height, spriteRect.width, spriteRect.height);
		background.graphics.endFill();
		
		qt = new QuadTree(new Rectangle(0, 0, BOUNDS.screenWidth, BOUNDS.screenHeight));
		
		//So, things here, the Factories, get the reference to the render list.
		ActorFactory.Initalize(render.renderList, sceneManager, spriteAssets, qt);
		
		REF = this;
		
		//Initalize required inputs
		#if android
			mouse = new Pointer(PointerType.Touch, BOUNDS);
			keyboard = null;
			
			//For now, software joystick
			moveStick = new TouchJoystiq(mouse);
			moveStick.x = BOUNDS.screenToGameX(moveStick.range + 10);
			moveStick.y = BOUNDS.screenToGameY(stage.stageHeight - moveStick.range - 10);
			gameStage.addChild(moveStick);
			
			turnStick = new TouchJoystiq(mouse);
			turnStick.preserveOnRelease = true;
			turnStick.x = BOUNDS.screenToGameX(stage.stageWidth - turnStick.range - 10);
			turnStick.y = BOUNDS.screenToGameY(stage.stageHeight - turnStick.range - 10);
			gameStage.addChild(turnStick);
		#else
			keyboard = new ButtonInput(ButtonInputTypes.Keyboard);
			mouse = new Pointer(PointerType.Mouse, BOUNDS);
		#end
		
		/* THE FOLLOWING ASSETS ARE NOT LEVEL SPECIFIC. */
		gameMenus = new MainMenu();
		gameMenus.loadMainMenu();
		
		Levelerator = new Level(sceneManager);
		Levelerator.GenerateLevel(Level.LEVEL_MAIN);
		
		sceneManager.addTickable(gameMenus);
		sceneManager.addTickable(Levelerator);
		
		//Scale shit up
		gameStage.x = BOUNDS.offsetLeft;
		gameStage.y = BOUNDS.offsetTop;
		gameStage.scaleX = gameStage.scaleY = BOUNDS.scaleFactor;
	}
	
	//Requires you declare render and assetManager to be set.
	public function loadAssets():Void {
		var jsonParse:Dynamic;
		var sheetData:String = Assets.getText("dat/spriteSheet.json");
		jsonParse = Json.parse(sheetData);
		var fields:Array<String> = Reflect.fields(jsonParse.frames);
		spriteAssets = new Hash<SpriteAsset>();
		for (assetName in fields) {
			var spriteData:Dynamic = Reflect.getProperty(jsonParse.frames, assetName);
			var rect:Rectangle = new Rectangle(spriteData.frame.x, spriteData.frame.y, spriteData.frame.w, spriteData.frame.h);
			var center:Point = new Point(spriteData.centerPtTwo.x / 2, spriteData.centerPtTwo.y / 2);
			spriteAssets.set(assetName, assetManager.loadSpriteAsset(assetName, render, rect, center));
		}
	}
	
	public function getSpriteAsset(assetName:String):SpriteAsset {
		return spriteAssets.get(assetName);
	}
	
	//Start the game
	public function start():Void {
		gameStartTime = Lib.getTimer() / 1000;
		sceneManager.isPaused = false;//Unpause the game
	}
	
}