package com.ssjskipp.games.hammersmash.game.misc;
import com.ssjskipp.games.hammersmash.bazaar.debug.Debug;
import com.ssjskipp.games.hammersmash.bazaar.debug.DebugChannels;
import nme.net.SharedObject;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Keeps track of all the wonderful statistics that are involved in the game.
 * 
 * Static class.
 * 
 * Since the internal hash is dynamic, its up to you to know your types.
 */
class GameStatistics {
	
	public static inline var STAT_BOXES_BROKE_INT:String = "boxesBroke";
	public static inline var STAT_ENEMIES_BROKE_INT:String = "enemiesBroke";
	public static inline var STAT_HIGHEST_SCORE_INT_ARR:String = "highestScore";
	public static inline var STAT_ENEMY_KILL_LEVEL_INT:String = "enemyKillLevel";
	public static inline var STAT_BOX_KILL_LEVEL_INT:String = "boxKillLevel";
	public static inline var STAT_SCORE_LEVEL_INT:String = "scoreLevel";
	public static inline var STAT_TIMES_DEAD_INT:String = "timesDead";
	
	static inline var DATA_INIT_CHECK:String = "dataInit";
	static inline var SAVE_GAME:String = "boomBoxBreakSave";
	
	public static var saveObject(getSharedObject, null):SharedObject = null;
	
	static var internalSharedObject:SharedObject = null;
	
	public function new() {
		
	}
	
	public static function init():Void {
		if (internalSharedObject == null){
			internalSharedObject = SharedObject.getLocal(SAVE_GAME);
			var isInit:Bool = cast Reflect.getProperty(internalSharedObject.data, DATA_INIT_CHECK);
			if (isInit == false) {
				setStatistic(DATA_INIT_CHECK, true);
				setStatistic(STAT_BOXES_BROKE_INT, 0);
				setStatistic(STAT_ENEMIES_BROKE_INT, 0);
				setStatistic(STAT_ENEMY_KILL_LEVEL_INT, 0);
				setStatistic(STAT_BOX_KILL_LEVEL_INT, 0);
				setStatistic(STAT_SCORE_LEVEL_INT, 0);
				setStatistic(STAT_TIMES_DEAD_INT, 0);
				setStatistic(STAT_HIGHEST_SCORE_INT_ARR, new Array<Int>());
			}
			Debug.log(DebugChannels.GAMEPLAY, DATA_INIT_CHECK+" = "+isInit);
			Debug.log(DebugChannels.GAMEPLAY, STAT_BOXES_BROKE_INT+" = "+getStatistic(STAT_BOXES_BROKE_INT));
			Debug.log(DebugChannels.GAMEPLAY, STAT_ENEMIES_BROKE_INT+" = "+getStatistic(STAT_ENEMIES_BROKE_INT));
			Debug.log(DebugChannels.GAMEPLAY, STAT_HIGHEST_SCORE_INT_ARR+" = "+getStatistic(STAT_HIGHEST_SCORE_INT_ARR));
		}
	}
	
	public static function getSharedObject():SharedObject {
		if (internalSharedObject == null) init();
		return internalSharedObject;
	}
	
	public static function saveData():Void {
		if (internalSharedObject == null) init();
		internalSharedObject.flush();
	}
	
	//It's up to you to type it!
	public static function getStatistic(name:String):Dynamic {
		if (internalSharedObject == null) init();
		return Reflect.getProperty(internalSharedObject.data, name);
	}
	public static function setStatistic(name:String, value:Dynamic):Void {
		if (internalSharedObject == null) init();
		Reflect.setProperty(internalSharedObject.data, name, value);
	}
	
	//Modify an integer statistic. Can add negative numbers.
	public static function postAddIntStatistic(name:String, toAdd:Int):Void {
		if (internalSharedObject == null) init();
		var intData:Int = 0;
		if (Reflect.hasField(internalSharedObject.data, name)) intData = cast getStatistic(name);// No assumptinos. Although a null pointer should then set to 0 on cast to int in any reasonable language.
		intData += toAdd;
		setStatistic(name, intData);
	}
	
	//Modify an integer statistic. Can add negative numbers.
	public static function postAddFloatStatistic(name:String, toAdd:Float):Void {
		if (internalSharedObject == null) init();
		var floatData:Float = 0;
		if (Reflect.hasField(internalSharedObject.data, name)) floatData = cast getStatistic(name);
		floatData += toAdd;
		setStatistic(name, floatData);
	}
	
	
}