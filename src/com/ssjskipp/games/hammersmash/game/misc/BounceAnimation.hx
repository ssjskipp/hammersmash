package com.ssjskipp.games.hammersmash.game.misc;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import com.ssjskipp.games.hammersmash.bazaar.misc.IPositionable;
import com.ssjskipp.games.hammersmash.bazaar.state.State;

/**
 * ...
 * @author Chris Federici
 * 
 * Assumes the target has a 'y' property
 */
class BounceAnimation extends State, implements ITickable
{
	public static inline var STATE_ID:String = "BounceAnimation";
	
	public static inline var LAST:String = "lastBounce";
	public static inline var PAUSE:String = "pauseBounce";
	public static inline var PLAY:String = "playBounce";
	
	public var enabled:Bool = true;
	
	var height:Float = 10;
	var period:Float = 1;
	var time:Float = 0;
	
	var initialY:Float = 0;
	
	var theta:Float = 0;
	var delay:Float = 0;
	
	//Period and delay in SECONDS
	public function new(targetObject:IPositionable, ?bounceHeight:Float, ?period:Float, ?delay:Float = 0) 
	{
		super(targetObject);
		name = PAUSE;
		if (bounceHeight != null) height = bounceHeight;
		if (delay != null) this.delay = delay;
		if (period != null) this.period = Math.max(0, period);
		
		id = STATE_ID;
	}
	
	override public function enterState():Void {
		var objectAsPositionable:IPositionable = cast target;
		if (name == PLAY) {
			initialY = objectAsPositionable.y;
		}
	}
	
	override public function tick(dt:Float, step:Float):Void {
		var objectAsPositionable:IPositionable = cast target;
		if (name == PLAY || name == LAST) {
			if (time < 0) {
				time = Math.min(time + dt, 0);//Delay
			} else {
				time = Math.min(time + dt, period);
				theta = (time / period) * Math.PI;
				objectAsPositionable.y = initialY - Math.sin(theta) * height;
				if (time == period) {
					if (name != PLAY) {
						time = 0;
						changeState(PAUSE);
					} else {
						time = -delay;
					}
				}
			}
		}
	}
	
}