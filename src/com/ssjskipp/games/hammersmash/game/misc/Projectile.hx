package com.ssjskipp.games.hammersmash.game.misc;
import com.ssjskipp.games.hammersmash.bazaar.collide.ICollidable;
import com.ssjskipp.games.hammersmash.bazaar.collide.quadtree.IQuadTreeItem;
import com.ssjskipp.games.hammersmash.bazaar.collide.quadtree.QuadTree;
import com.ssjskipp.games.hammersmash.bazaar.controllers.ButtonInput;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import com.ssjskipp.games.hammersmash.bazaar.misc.IPositionable;
import com.ssjskipp.games.hammersmash.bazaar.render.TileRenderItem;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.bazaar.scene.SceneManager;
import com.ssjskipp.games.hammersmash.bazaar.state.StateController;
import com.ssjskipp.games.hammersmash.game.HammerSmashGame;
import com.ssjskipp.games.hammersmash.game.levels.Level;
import nme.events.Event;
import nme.geom.Point;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * This seems kind of gross...
 */
class Projectile extends Actor, implements ITickable {
	
	public static inline var ID:String = "Projectile";
	
	public var enabled:Bool = true;
	public var lifetime:Float;
	public var damage:Int = 1;
	
	public function new(tileSprite:TileRenderItem, parentScene:SceneManager) {
		super(tileSprite);
		enableProjectilePhysics();
		parentScene.addTickable(this);
		lifetime = 0;
		Level.REF.projectiles.push(this);
	}
	
	public function tick(delta:Float, step:Float):Void {
		lifetime -= delta;
		if (lifetime <= 0) {
			destroy();
		} else {
			projectilePhysics.tick(delta, step);
			setPosition(projectilePhysics.position.x, projectilePhysics.position.y);
			if (x < HammerSmashGame.BOUNDS.left || y < HammerSmashGame.BOUNDS.top || y > HammerSmashGame.BOUNDS.bottom || x > HammerSmashGame.BOUNDS.right) {
				destroy();
			}
		}
	}
	
	override public function destroy():Void {
		Level.REF.particles.addParticles(x, y, 2);
		enabled = false;
		if (parent != null) {
			parent.removeTickable(this);
		}
		super.destroy();
	}
	
}