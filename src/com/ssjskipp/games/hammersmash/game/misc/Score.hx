package com.ssjskipp.games.hammersmash.game.misc;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.game.actors.Enemy;
import nme.Lib;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Static class to help make score calculations better.
 */
class Score {
	
	static inline var ENEMY_VALUE:Int = 10;
	
	public static var lastBoxKill:Int = 0;
	public static var currentBoxCombo:Int = 1;
	public static var currentScore:Int = 0;
	
	public function new() {
		
	}
	
	public static function resetScore():Void {
		currentScore = 0;
		currentBoxCombo = 1;
		lastBoxKill = 0;
	}
	
	public static function postBreakCombo():Bool {
		var out:Bool = true;
		if (currentBoxCombo <= 1) out = false;
		currentBoxCombo = 1;
		return out;
	}
	
	public static function postEnemyKill(?enemy:Enemy):Void {
		var currentTime:Int = Lib.getTimer();
		currentScore += ENEMY_VALUE * currentBoxCombo;
	}
	
	public static function postBoxBreak(?box:Actor):Void {
		var currentTime:Int = Lib.getTimer();
		currentBoxCombo++;
		lastBoxKill = currentTime;
	}
	
}