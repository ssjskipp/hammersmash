package com.ssjskipp.games.hammersmash.game.ui;
import com.ssjskipp.games.hammersmash.bazaar.animation.TransitionPlayer;
import com.ssjskipp.games.hammersmash.bazaar.animation.transitions.BaseTransition;
import com.ssjskipp.games.hammersmash.bazaar.animation.transitions.BounceFx;
import com.ssjskipp.games.hammersmash.bazaar.animation.transitions.FadeFx;
import com.ssjskipp.games.hammersmash.bazaar.animation.transitions.SlideFx;
import com.ssjskipp.games.hammersmash.bazaar.events.DataEvent;
import com.ssjskipp.games.hammersmash.bazaar.maths.Interpolator;
import com.ssjskipp.games.hammersmash.bazaar.render.TileRenderer;
import com.ssjskipp.games.hammersmash.bazaar.render.TileRenderList;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.bazaar.text.TextBox;
import com.ssjskipp.games.hammersmash.bazaar.ui.component.Button;
import com.ssjskipp.games.hammersmash.bazaar.ui.Menu;
import com.ssjskipp.games.hammersmash.bazaar.ui.UIEvent;
import com.ssjskipp.games.hammersmash.game.factories.ActorFactory;
import com.ssjskipp.games.hammersmash.game.HammerSmashGame;
import com.ssjskipp.games.hammersmash.game.levels.Level;
import com.ssjskipp.games.hammersmash.game.misc.GameStatistics;
import com.ssjskipp.games.hammersmash.game.misc.Score;
import nme.filters.GlowFilter;
import nme.text.TextFormatAlign;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * This is every menu.
 * 
 * I should split this into not every menu.
 * 
 * TODO: Adding buttons + listeners to a more mediated function/interface.
 * 
 * I HAVE A LOT OF REPLICATED MENU "DESIGNS". Lots of recycling.
 * 
 * Common menu tasks I'm noticing:
	 * Have items/components (buttons)
	 * + Has menu locations
	 * + Has transitions
	 * + Has events
	 * When pressing a button, the menu transitions out then loads a new menu or does a sepecific action. Like generating a level
	 * Look for patterns in this class, use those patterns to streamline menu development in the future.
 */
class MainMenu extends Menu {
	
	static inline var NONE:Int = -1;
	static inline var MAIN:Int = 0;
	static inline var GAME_OVER:Int = 1;
	static inline var LEVEL_WIN:Int = 2;
	
	public var title:Actor;
	var titleBtn:Button;
	
	public var playGfx:Actor;
	var playBtn:Button;
	
	public var mainMenuGfx:Actor;
	var mainMenuBtn:Button;
	
	var currentMenu:Int;
	
	var dispalyText:TextBox;
	
	public function new() {
		super();
		
		currentMenu = NONE;
		
		dispalyText = new TextBox();
		#if flash
			dispalyText.setFormat(TextBox.FONT_ANAHEIM, 25, 0xCCCCCC, true, TextFormatAlign.CENTER);
		#else 
			dispalyText.setFormat(TextBox.FONT_ANAHEIM, 25, 0xCCCCCC, true, null, native.text.TextFormatAlign.CENTER);
		#end
		dispalyText.setDimensions(HammerSmashGame.BOUNDS.left + (1 / 6) * HammerSmashGame.BOUNDS.width, MAIN_MENU_Y + 50, (2 / 3) * HammerSmashGame.BOUNDS.width, HammerSmashGame.BOUNDS.height - MAIN_MENU_Y - 50);
		dispalyText.graphics.beginFill(0x000000, 0.75);
		dispalyText.graphics.drawRect(0, 0, dispalyText.width, dispalyText.height);
		dispalyText.graphics.endFill();
		dispalyText.alpha = 0;
		
		menuTransitions.addEventListener(TransitionPlayer.TRANSITION_COMPLETE, transitionListener);
	}
	
	/* This stuff should be moved into data driven stuff. Code end now because I don't have a language to translate it in. */
	var bounceFlag:Bool = false;
	
	static inline var TITLE_BOUNCE:String = "titleBounce";
	
	public static inline var TITLE_Y:Float = 100;
	public static inline var PLAY_Y:Float = 170;
	public static inline var MAIN_MENU_Y:Float = 230;
	
	static inline var CHOICE_PLAY:Int = 0;
	static inline var CHOICE_MAINMENU:Int = 1;
	static inline var CHOICE_NEXTLEVEL:Int = 2;
	
	var menuChoice:Int;
	
	var menuButtonsEnabled:Bool = true;
	
	//EVERYTHING BELOW HERE DESCRIBES EVENTS PERTAINING TO MENUS
	
	private function transitionListener(e:DataEvent):Void {
		var targetAsTransition:BaseTransition;
		if (e.type == TransitionPlayer.TRANSITION_COMPLETE) {
			targetAsTransition = cast e.dataValue;
			//Titles bounce.
			if (targetAsTransition.name == TITLE_BOUNCE) {
				bounceFlag = false;
			}
			//Main menu items
			if (currentMenu == MAIN && menuChoice == CHOICE_PLAY) {
				unloadMainMenu();
				HammerSmashGame.REF.Levelerator.GenerateLevel(Level.LEVEL_1);
			}
			//Game over menu items
			if (currentMenu == GAME_OVER && menuChoice == CHOICE_PLAY) {
				unloadGameOverMenu();
				HammerSmashGame.REF.Levelerator.GenerateLevel(Level.LEVEL_1);
			}
			if (currentMenu == GAME_OVER && menuChoice == CHOICE_MAINMENU) {
				unloadGameOverMenu();
				loadMainMenu();
				HammerSmashGame.REF.Levelerator.GenerateLevel(Level.LEVEL_MAIN);
			}
			//Level-win menu items
			if (currentMenu == LEVEL_WIN && menuChoice == CHOICE_NEXTLEVEL) {
				unloadLevelOverMenu();
				HammerSmashGame.REF.Levelerator.GenerateLevel(Level.LEVEL_1);
			}
			if (currentMenu == LEVEL_WIN && menuChoice == CHOICE_MAINMENU) {
				unloadLevelOverMenu();
				loadMainMenu();
				HammerSmashGame.REF.Levelerator.GenerateLevel(Level.LEVEL_MAIN);
			}
		}
	}
	
	private function mainMenuButton(e:UIEvent):Void {
		var bounceTransition:BounceFx;
		var titleTransitionOut:SlideFx;
		var playTransitionOut:SlideFx;
		var mainMenuTransitionOut:SlideFx;
		var fadeTextOut:FadeFx;
		
		if (e.targetUIComp == titleBtn && bounceFlag == false) {
			bounceFlag = true;
			bounceTransition = new BounceFx(1, e.targetUIComp.graphic, e.targetUIComp.graphic.y, e.targetUIComp.graphic.y - 45, Interpolator.EASE_QUINT_OUT, Interpolator.EASE_CIRC_IN);
			bounceTransition.name = TITLE_BOUNCE;
			menuTransitions.playTransition(bounceTransition);
		}
		
		//Below are transition choices
		if (!menuButtonsEnabled) return;
		
		menuChoice = -1;
		if (e.targetUIComp == playBtn && currentMenu == MAIN) {
			menuChoice = CHOICE_PLAY;
		} else if (currentMenu == GAME_OVER && e.targetUIComp == playBtn) {
			menuChoice = CHOICE_PLAY;
		} else if (currentMenu == GAME_OVER && e.targetUIComp == mainMenuBtn) {
			menuChoice = CHOICE_MAINMENU;
		} else if (currentMenu == LEVEL_WIN && e.targetUIComp == mainMenuBtn) {
			menuChoice = CHOICE_MAINMENU;
		} else if (currentMenu == LEVEL_WIN && e.targetUIComp == playBtn) {
			menuChoice = CHOICE_NEXTLEVEL;
		}
		
		if (menuChoice != -1) {
			menuButtonsEnabled = false;
			
			if (currentMenu == MAIN || currentMenu == GAME_OVER || currentMenu == LEVEL_WIN) {
				fadeTextOut = new FadeFx(1, dispalyText, 0, Interpolator.EASE_QUART_IN_OUT);
				titleTransitionOut = new SlideFx(1, title, HammerSmashGame.BOUNDS.centerX, HammerSmashGame.BOUNDS.worldHeight + title.height, Interpolator.EASE_QUART_IN_OUT);
				playTransitionOut = new SlideFx(1, playGfx, HammerSmashGame.BOUNDS.worldWidth + playGfx.width, PLAY_Y, Interpolator.EASE_QUART_IN_OUT);
				menuTransitions.playTransition(titleTransitionOut);
				menuTransitions.playTransition(playTransitionOut);
				menuTransitions.playTransition(fadeTextOut);
			}
			
			if (currentMenu == MAIN) {
				Level.REF.explodeAllBoxes();
			} else if (currentMenu == GAME_OVER || currentMenu == LEVEL_WIN) {
				mainMenuTransitionOut = new SlideFx(1, mainMenuGfx, HammerSmashGame.BOUNDS.screenToGameX(-playGfx.width), MAIN_MENU_Y, Interpolator.EASE_QUART_IN_OUT);
				menuTransitions.playTransition(mainMenuTransitionOut);
			}
		}
		
	}
	
	//EVERYTHING BELOW HERE DESCRIBES THE MENUS.
	
	public function unloadMainMenu():Void {
		currentMenu = NONE;
		
		menuButtonsEnabled = false;
		
		removeUiComponent(titleBtn);
		removeUiComponent(playBtn);
		
		HammerSmashGame.REF.gameStage.removeChild(dispalyText);
		
		titleBtn.removeEventListener(UIEvent.BUTTON_CLICK, mainMenuButton);
		playBtn.removeEventListener(UIEvent.BUTTON_CLICK, mainMenuButton);
		
		titleBtn.destroy();
		playBtn.destroy();
	}
	
	public function loadMainMenu():Void {
		if (currentMenu != NONE) return;
		
		menuButtonsEnabled = true;
		
		title = ActorFactory.BuildActor(ActorFactory.GAME_LOGO, HammerSmashGame.LAYER_UI);
		title.x = HammerSmashGame.BOUNDS.centerX;
		title.y = -title.height;
		titleBtn = new Button(title, title, HammerSmashGame.REF.mouse);
		
		playGfx = ActorFactory.BuildActor(ActorFactory.PLAY_GAME_BTN, HammerSmashGame.LAYER_UI);
		playBtn = new Button(playGfx, playGfx, HammerSmashGame.REF.mouse);
		playGfx.x = title.x;
		playGfx.y = -playGfx.height;
		
		var titleTransitionIn:SlideFx = new SlideFx(1, title, HammerSmashGame.BOUNDS.centerX, TITLE_Y, Interpolator.EASE_QUART_IN_OUT);
		var playTransitionIn:SlideFx = new SlideFx(1, playGfx, HammerSmashGame.BOUNDS.centerX, PLAY_Y, Interpolator.EASE_QUART_IN_OUT);
		var fadeTextIn:FadeFx = new FadeFx(1, dispalyText, 1, Interpolator.EASE_QUART_IN_OUT);
		
		titleBtn.addEventListener(UIEvent.BUTTON_CLICK, mainMenuButton);
		playBtn.addEventListener(UIEvent.BUTTON_CLICK, mainMenuButton);
		
		var introStr:String = "Welcome to BoomBoxBreak.\n";
		introStr += "Get the highest score without dieing.\n";
		introStr += "Break crates for combo. Break enemies to win.\n";
		introStr += "Enemies breaking crates breaks combo.\n";
		#if android
			introStr += "Use the Joysticks on the left and right.\n";
		#else
			introStr += "W, A, S, D and Mouse to move and aim.\n";
		#end
		dispalyText.setText(introStr);
		
		HammerSmashGame.REF.gameStage.addChild(dispalyText);
		
		menuTransitions.playTransition(titleTransitionIn);
		menuTransitions.playTransition(playTransitionIn);
		menuTransitions.playTransition(fadeTextIn);
		
		currentMenu = MAIN;
		menuChoice = -1;
		
		addUIComponent(titleBtn);
		addUIComponent(playBtn);
	}
	
	public function unloadGameOverMenu():Void {
		currentMenu = NONE;
		
		menuButtonsEnabled = false;
		
		removeUiComponent(titleBtn);
		removeUiComponent(playBtn);
		removeUiComponent(mainMenuBtn);
		
		HammerSmashGame.REF.gameStage.removeChild(dispalyText);
		
		titleBtn.removeEventListener(UIEvent.BUTTON_CLICK, mainMenuButton);
		playBtn.removeEventListener(UIEvent.BUTTON_CLICK, mainMenuButton);
		mainMenuBtn.removeEventListener(UIEvent.BUTTON_CLICK, mainMenuButton);
		
		titleBtn.destroy();
		playBtn.destroy();
		mainMenuBtn.destroy();
	}
	
	public function loadGameOverMenu():Void {
		if (currentMenu != NONE) return;
		
		menuButtonsEnabled = true;
		
		title = ActorFactory.BuildActor(ActorFactory.GAME_OVER_TEXT, HammerSmashGame.LAYER_UI);
		title.x = HammerSmashGame.BOUNDS.centerX;
		title.y = -title.height;
		titleBtn = new Button(title, title, HammerSmashGame.REF.mouse);
		
		playGfx = ActorFactory.BuildActor(ActorFactory.PLAY_AGAIN_BTN, HammerSmashGame.LAYER_UI);
		playGfx.x = title.x;
		playGfx.y = -playGfx.height;
		playBtn = new Button(playGfx, playGfx, HammerSmashGame.REF.mouse);
		
		mainMenuGfx = ActorFactory.BuildActor(ActorFactory.MAIN_MENU_BTN, HammerSmashGame.LAYER_UI);
		mainMenuGfx.x = playGfx.x;
		mainMenuGfx.y = -mainMenuGfx.height;
		mainMenuBtn = new Button(mainMenuGfx, mainMenuGfx, HammerSmashGame.REF.mouse);
		
		currentMenu = GAME_OVER;
		menuChoice = -1;
		
		var titleTransitionIn:SlideFx = new SlideFx(1, title, HammerSmashGame.BOUNDS.centerX, TITLE_Y, Interpolator.EASE_QUART_IN_OUT);
		var playTransitionIn:SlideFx = new SlideFx(1, playGfx, HammerSmashGame.BOUNDS.centerX, PLAY_Y, Interpolator.EASE_QUART_IN_OUT);
		var menuTransitionIn:SlideFx = new SlideFx(1, mainMenuGfx, HammerSmashGame.BOUNDS.centerX, MAIN_MENU_Y, Interpolator.EASE_QUART_IN_OUT);
		var fadeTextIn:FadeFx = new FadeFx(1, dispalyText, 1, Interpolator.EASE_QUART_IN_OUT);
		
		titleBtn.addEventListener(UIEvent.BUTTON_CLICK, mainMenuButton);
		playBtn.addEventListener(UIEvent.BUTTON_CLICK, mainMenuButton);
		mainMenuBtn.addEventListener(UIEvent.BUTTON_CLICK, mainMenuButton);
		
		var textToDisplay:String = "Your final score " + Score.currentScore;
		textToDisplay += "\nYou smashed " + GameStatistics.getStatistic(GameStatistics.STAT_BOX_KILL_LEVEL_INT) + " boxes that round,\n";
		textToDisplay += "and you popped " + GameStatistics.getStatistic(GameStatistics.STAT_ENEMY_KILL_LEVEL_INT) + " enemies.\n";
		dispalyText.setText(textToDisplay);
		HammerSmashGame.REF.gameStage.addChild(dispalyText);
		
		menuTransitions.playTransition(titleTransitionIn);
		menuTransitions.playTransition(playTransitionIn);
		menuTransitions.playTransition(menuTransitionIn);
		menuTransitions.playTransition(fadeTextIn);
		
		addUIComponent(titleBtn);
		addUIComponent(playBtn);
		addUIComponent(mainMenuBtn);
	}
	
	public function unloadLevelOverMenu():Void {
		currentMenu = NONE;
		
		menuButtonsEnabled = false;
		
		removeUiComponent(titleBtn);
		removeUiComponent(playBtn);
		removeUiComponent(mainMenuBtn);
		
		HammerSmashGame.REF.gameStage.removeChild(dispalyText);
		
		titleBtn.removeEventListener(UIEvent.BUTTON_CLICK, mainMenuButton);
		playBtn.removeEventListener(UIEvent.BUTTON_CLICK, mainMenuButton);
		mainMenuBtn.removeEventListener(UIEvent.BUTTON_CLICK, mainMenuButton);
		
		titleBtn.destroy();
		playBtn.destroy();
		mainMenuBtn.destroy();
	}
	
	public function loadLevelOverMenu():Void {
		if (currentMenu != NONE) return;
		
		menuButtonsEnabled = true;
		
		title = ActorFactory.BuildActor(ActorFactory.LEVEL_WIN, HammerSmashGame.LAYER_UI);
		title.x = HammerSmashGame.BOUNDS.centerX;
		title.y = -title.height;
		titleBtn = new Button(title, title, HammerSmashGame.REF.mouse);
		
		playGfx = ActorFactory.BuildActor(ActorFactory.NEXT_LEVEL_BTN, HammerSmashGame.LAYER_UI);
		playGfx.x = title.x;
		playGfx.y = -playGfx.height;
		playBtn = new Button(playGfx, playGfx, HammerSmashGame.REF.mouse);
		
		mainMenuGfx = ActorFactory.BuildActor(ActorFactory.MAIN_MENU_BTN, HammerSmashGame.LAYER_UI);
		mainMenuGfx.x = playGfx.x;
		mainMenuGfx.y = -mainMenuGfx.height;
		mainMenuBtn = new Button(mainMenuGfx, mainMenuGfx, HammerSmashGame.REF.mouse);
		
		currentMenu = LEVEL_WIN;
		menuChoice = -1;
		
		var titleTransitionIn:SlideFx = new SlideFx(1, title, HammerSmashGame.BOUNDS.centerX, TITLE_Y, Interpolator.EASE_QUART_IN_OUT);
		var playTransitionIn:SlideFx = new SlideFx(1, playGfx, HammerSmashGame.BOUNDS.centerX, PLAY_Y, Interpolator.EASE_QUART_IN_OUT);
		var menuTransitionIn:SlideFx = new SlideFx(1, mainMenuGfx, HammerSmashGame.BOUNDS.centerX, MAIN_MENU_Y, Interpolator.EASE_QUART_IN_OUT);
		var fadeTextIn:FadeFx = new FadeFx(1, dispalyText, 1, Interpolator.EASE_QUART_IN_OUT);
		
		titleBtn.addEventListener(UIEvent.BUTTON_CLICK, mainMenuButton);
		playBtn.addEventListener(UIEvent.BUTTON_CLICK, mainMenuButton);
		mainMenuBtn.addEventListener(UIEvent.BUTTON_CLICK, mainMenuButton);
		
		var textToDisplay:String = "Level win! Current Score " + Score.currentScore;
		textToDisplay += "\nYou smashed " + GameStatistics.getStatistic(GameStatistics.STAT_BOX_KILL_LEVEL_INT) + " boxes that round,\n";
		textToDisplay += "and you popped " + GameStatistics.getStatistic(GameStatistics.STAT_ENEMY_KILL_LEVEL_INT) + " enemies.\n";
		dispalyText.setText(textToDisplay);
		HammerSmashGame.REF.gameStage.addChild(dispalyText);
		
		menuTransitions.playTransition(titleTransitionIn);
		menuTransitions.playTransition(playTransitionIn);
		menuTransitions.playTransition(menuTransitionIn);
		menuTransitions.playTransition(fadeTextIn);
		
		addUIComponent(titleBtn);
		addUIComponent(playBtn);
		addUIComponent(mainMenuBtn);
	}
	
}