package com.ssjskipp.games.hammersmash.game.weapon;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Anythig that can weild a weapon. So, it has an actor.
 */
interface IWeaponHolder {

	public var actor:Actor;
	
}