package com.ssjskipp.games.hammersmash.game.weapon;
import com.ssjskipp.games.hammersmash.bazaar.debug.Debug;
import com.ssjskipp.games.hammersmash.bazaar.debug.DebugChannels;
import com.ssjskipp.games.hammersmash.bazaar.misc.ITickable;
import com.ssjskipp.games.hammersmash.bazaar.maths.Vector;
import com.ssjskipp.games.hammersmash.bazaar.render.TileLayer;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.game.HammerSmashGame;
import com.ssjskipp.games.hammersmash.game.levels.Level;
import nme.events.EventDispatcher;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Grenade weapon. Just to test.
 */
class Grenade implements ITickable {
	
	public var explosionRadius:Float = 40;
	public var actor:Actor;
	
	public var enabled:Bool = true;
	
	var lifetime:Float;
	var accelerationMagnitude:Float;
	
	public function new(actor:Actor, throwDirection:Vector, throwSpeed:Float) {
		this.actor = actor;
		actor.enableProjectilePhysics();
		actor.projectilePhysics.acceleration.x = -throwDirection.x * (throwSpeed / 75);
		actor.projectilePhysics.acceleration.y = -throwDirection.y * (throwSpeed / 75);
		actor.projectilePhysics.velocity.x = throwDirection.x * throwSpeed;
		actor.projectilePhysics.velocity.y = throwDirection.y * throwSpeed;
		accelerationMagnitude = actor.projectilePhysics.acceleration.getMagnitude();
		lifetime = 4;
	}
	
	public function tick(delta:Float, step:Float):Void {
		if (actor.projectilePhysics.velocity.getMagnitude() >= accelerationMagnitude) {
			actor.projectilePhysics.tick(delta, step);
		}
		actor.x = actor.projectilePhysics.position.x;
		actor.y = actor.projectilePhysics.position.y;
		
		lifetime -= delta;
		
		if (lifetime <= 0) {
			explode();
		}
		
		//Draw blow up ring
		if (lifetime <= 1 && !actor.isDestroyed) {
			HammerSmashGame.REF.gameApi.graphics.beginFill(0xFF0000,  Math.min(Math.abs(lifetime - 1), 0.75));
			HammerSmashGame.REF.gameApi.graphics.drawCircle(actor.x, actor.y, explosionRadius);
			HammerSmashGame.REF.gameApi.graphics.endFill();
		}
	}
	
	//Need to determine how to do this better
	public function explode():Void {
		//Handle my shit
		if (!actor.isDestroyed && enabled){
			actor.radius = explosionRadius;
			Level.REF.particles.addParticles(actor.x, actor.y, Math.round(Math.random() * 10 + 15));
			Level.REF.player.collideWith(actor.x, actor.y, actor);
			Level.REF.smashBoxController.collideWith(actor.x, actor.y, explosionRadius);
			destroy();
		}
	}
	
	public function destroy():Void {
		HammerSmashGame.REF.sceneManager.removeTickable(this);
		if (!actor.isDestroyed) actor.destroy();
		enabled = false;
	}
	
}