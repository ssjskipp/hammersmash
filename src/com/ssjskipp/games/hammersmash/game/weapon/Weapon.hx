package com.ssjskipp.games.hammersmash.game.weapon;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.game.factories.ActorFactory;
import com.ssjskipp.games.hammersmash.game.HammerSmashGame;
import com.ssjskipp.games.hammersmash.game.misc.Projectile;
import nme.Lib;

/**
 * ...
 * @author Chris Federici
 * 
 * Player weapon
 */
 
class Weapon {
	
	//Cooldown in milliseconds
	var cooldown:Int = 250;
	var lastFire:Int = 0;
	
	var holder:Actor;
	
	static var newProjectileHash:Hash<String> = null;
	
	public function new(holder:Actor) {
		this.holder = holder;
		
		if (newProjectileHash == null) {
			newProjectileHash = new Hash<String>();
			newProjectileHash.set(ActorFactory.PROP_USE_QUAD_TREE, "true");
			newProjectileHash.set(ActorFactory.PROP_QUADTREE_ITEM_ID, Projectile.ID);
		}
	}
	
	public function fire():Void {
		var happyHackyFloat:Float;
		
		var currentTime:Int = Lib.getTimer();
		if (lastFire + cooldown <= currentTime) {
			lastFire = currentTime;
			
			var newBullet:Projectile = cast ActorFactory.BuildActor(ActorFactory.PIXEL_BLAST, HammerSmashGame.LAYER_PARTICLES, ActorFactory.TYPE_PROJECTILE, newProjectileHash);
			newBullet.lifetime = 2;
			newBullet.x = newBullet.projectilePhysics.position.x = holder.x;
			newBullet.y = newBullet.projectilePhysics.position.y = holder.y;
			happyHackyFloat = holder.rotation + (Math.random() > 0.5 ? 1 : -1) * Math.random() * Math.PI / 27;
			newBullet.projectilePhysics.velocity.x = Math.cos(happyHackyFloat) * 5;
			newBullet.projectilePhysics.velocity.y = -Math.sin(happyHackyFloat) * 5;
		}
	}
}
