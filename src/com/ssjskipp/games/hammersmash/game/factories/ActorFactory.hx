package com.ssjskipp.games.hammersmash.game.factories;
import com.ssjskipp.games.hammersmash.bazaar.assets.SpriteAsset;
import com.ssjskipp.games.hammersmash.bazaar.collide.quadtree.QuadTree;
import com.ssjskipp.games.hammersmash.bazaar.collide.quadtree.QuadTreeCollideBall;
import com.ssjskipp.games.hammersmash.bazaar.debug.Debug;
import com.ssjskipp.games.hammersmash.bazaar.debug.DebugChannels;
import com.ssjskipp.games.hammersmash.bazaar.render.TileRenderer;
import com.ssjskipp.games.hammersmash.bazaar.render.TileRenderItem;
import com.ssjskipp.games.hammersmash.bazaar.render.TileRenderList;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.bazaar.scene.SceneManager;
import com.ssjskipp.games.hammersmash.game.actors.Enemy;
import com.ssjskipp.games.hammersmash.game.HammerSmashGame;
import com.ssjskipp.games.hammersmash.game.misc.Projectile;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Mediates building an actor.
 */
class ActorFactory {
	
	//List of sprite graphics you can use
	public static inline var BOX_SPRITE:String = "boxSprite";
	public static inline var SMASHBOX_SPRITE:String = "smashBoxSprite";
	public static inline var PIXEL_SPRITE_W:String = "whitePixelSprite";
	public static inline var PIXEL_SPRITE_R:String = "redPixelSprite";
	public static inline var PIXEL_SPRITE_G:String = "greenPixelSprite";
	public static inline var PIXEL_SPRITE_B:String = "bluePixelSprite";
	public static inline var WALL_SPRITE:String = "wallSpriteEdge";
	public static inline var ENEMY_SPRITE:String = "enemySprite";
	public static inline var GRENADE_SPRITE:String = "grenadeSprite";
	public static inline var GAME_LOGO:String = "gameLogo";
	public static inline var PIXEL_BLAST:String = "pixelBlastSprite";
	public static inline var PLAY_GAME_BTN:String = "startGameButton";
	public static inline var FLOOR_TILE:String = "floorTile";
	public static inline var WALL_CORNER:String = "wallCorner";
	public static inline var COMBO_METER_FACE:String = "comboMeterFace";
	public static inline var GAME_OVER_TEXT:String = "gameOverText";
	public static inline var NUM_ONE:String = "one";
	public static inline var NUM_TWO:String = "two";
	public static inline var NUM_THREE:String = "three";
	public static inline var LEVEL_WIN:String = "levelWin";
	public static inline var COMBO_BREAKER:String = "comboBroken";
	public static inline var MAIN_MENU_BTN:String = "mainMenuButton";
	public static inline var PLAY_AGAIN_BTN:String = "playAgainBtn";
	public static inline var NEXT_LEVEL_BTN:String = "nextLevelButton";
	
	//List of "types" of sprites to generate
	//Data requirements listed here as well
	public static inline var TYPE_ACTOR:String = "ACTOR";
	public static inline var TYPE_ENEMY:String = "ENEMY";
	public static inline var TYPE_PROJECTILE:String = "PROJECTILE";
	
	//List of the above mapped to their asset
	public static var spriteAssets:Hash<SpriteAsset>;
	public static var renderList:TileRenderList;
	public static var scene:SceneManager;
	public static var quadTree:QuadTree;
	
	public function new() {
		//SINGLETON
	}
	
	public static function Initalize(renderList:TileRenderList, sceneManager:SceneManager, assets:Hash<SpriteAsset>, quadTree:QuadTree):Void {
		ActorFactory.spriteAssets = assets;
		ActorFactory.scene = sceneManager;
		ActorFactory.quadTree = quadTree;
		ActorFactory.renderList = renderList;
	}
	
	//Useful for generating an animation list.
	public static function AssetList(assetNameArray:Array<String>):Array<SpriteAsset> {
		var out:Array<SpriteAsset> = new Array<SpriteAsset>();
		for (i in 0...assetNameArray.length) {
			var asset:SpriteAsset = spriteAssets.get(assetNameArray[i]);
			if (asset != null) {
				out.push(asset);
			} else {
				Debug.log(DebugChannels.DEBUG, "Error. No sprite asset by the name ("+assetNameArray[i]+")");
			}
		}
		return out;
	}
	
	//Pictured: Anti-pattern.
	//Here's some properties
	public static inline var PROP_X:String = "x";
	public static inline var PROP_Y:String = "y";
	public static inline var PROP_ROTATION:String = "rotation";
	public static inline var PROP_USE_QUAD_TREE:String = "useQuadTree";
	public static inline var PROP_QUADTREE_ITEM_ID:String = "itemId";
	//But not really. It's basically I need to create a tile sprite to use for an actor, and I need to get the sprite's info to make the tile sprite.
	//But all three of those classes need to exist on their own to be able to be portable.
	//It's like making a Sprite < Shape < Display Object, but I'm not EXACTLY right. But here's where building things boils down to.
	public static function BuildActor(spriteIdentifier:String, layer:Int, actorType:String = TYPE_ACTOR, ?data:Hash<String>):Actor {
		var asset:SpriteAsset = null;
		var renderItem:TileRenderItem = null;
		var out:Actor = null;
		//Common data members
		var x:Float = 0;
		var y:Float = 0;
		var rotation:Float = 0;
		var useQuadTree:Bool = false;
		var quadTreeId:String = actorType;
		
		if (data != null) {
			x = Std.parseFloat(data.get(PROP_X));
			y = Std.parseFloat(data.get(PROP_Y));
			rotation = Std.parseFloat(data.get(PROP_ROTATION));
			if (Math.isNaN(x)) x = 0;
			if (Math.isNaN(y)) y = 0;
			if (Math.isNaN(rotation)) rotation = 0;
			if (data.get(PROP_USE_QUAD_TREE) != null && data.get(PROP_USE_QUAD_TREE) == "true") {
				useQuadTree = true;
			}
			if (data.get(PROP_QUADTREE_ITEM_ID) != null) {
				quadTreeId = data.get(PROP_QUADTREE_ITEM_ID);
			}
		}
		
		asset = spriteAssets.get(spriteIdentifier);
		var renderItem:TileRenderItem = renderList.addNewRenderItem(asset, layer);
		
		if (actorType == TYPE_ACTOR) {
			out = new Actor(renderItem);
		} else if (actorType == TYPE_ENEMY) {
			out = new Enemy(renderItem, renderItem.spriteAsset.spriteRect);
		} else if (actorType == TYPE_PROJECTILE) {
			out = new Projectile(renderItem, scene);
		}
		if (out != null) {
			scene.addActor(out);
			if (useQuadTree) {
				out.quadTreeItem = new QuadTreeCollideBall(out);
				out.quadTreeItem.itemId = quadTreeId;
				quadTree.insert(out.quadTreeItem);
			}
			out.setPosition(x, y);
			//out.rotation = rotation;
		}
		
		return out;
	}
	
}