package com.ssjskipp.games.hammersmash.game.actors;
import com.ssjskipp.games.hammersmash.bazaar.maths.Vector;
import com.ssjskipp.games.hammersmash.bazaar.render.TileRenderItem;
import com.ssjskipp.games.hammersmash.bazaar.scene.Actor;
import com.ssjskipp.games.hammersmash.bazaar.state.StateController;
import com.ssjskipp.games.hammersmash.game.levels.Level;
//import com.ssjskipp.games.hammersmash.bazaar.maths.Rectangle;
import nme.geom.Rectangle;
import com.ssjskipp.games.hammersmash.game.factories.ActorFactory;
import com.ssjskipp.games.hammersmash.game.HammerSmashGame;
import com.ssjskipp.games.hammersmash.game.weapon.Grenade;
import nme.Lib;

/**
 * ...
 * @author Chris Federici (ssjskipp@gmail.com)
 * 
 * Enemy specific stuff
 */
class Enemy extends Actor
{
	
	public var aiStates:StateController;
	
	public var health:Float = 100;
	public var engageRange:Float = 300;
	public var maxHealth:Float = 100;
	public var range:Float = 200;
	public var speed:Float = 1.5;
	
	//THINGS FOR A WEAPON
	static inline var THROW_SPEED:Float = 5;
	var lastFire:Float = 0;
	var weaponCooldown:Float = 1;
	
	public function new(sprite:TileRenderItem, collisionRect:Rectangle) {
		super(sprite, collisionRect);
		aiStates = new StateController();
	}
	
	//This should be on a weapon, somehow.
	public function fireWeapon(at:Vector):Void {
		var curTime:Float = Lib.getTimer() / 1000;
		if (curTime - lastFire > weaponCooldown) {
			lastFire = curTime;
			var newGrenade:Grenade = new Grenade(ActorFactory.BuildActor(ActorFactory.GRENADE_SPRITE, HammerSmashGame.LAYER_PARTICLES, ActorFactory.TYPE_ACTOR), at, THROW_SPEED);
			newGrenade.actor.projectilePhysics.position.x = x;
			newGrenade.actor.projectilePhysics.position.y = y;
			HammerSmashGame.REF.sceneManager.addTickable(newGrenade);
			Level.REF.grenades.push(newGrenade);
		}
	}
}